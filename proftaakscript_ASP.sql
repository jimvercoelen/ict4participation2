SET SERVEROUTPUT ON; 
-- password: gRRnXgzAhb

drop table FILES cascade constraints;
drop table PERSON cascade constraints;
drop table PERSON_HELP_SEEKER cascade constraints;
drop table PERSON_VOLUNTEER cascade constraints;
drop table CHAT cascade constraints;
drop table PERSON_CHAT cascade constraints;
drop table MESSAGE cascade constraints;
drop table REVIEW cascade constraints;
drop table QUESTION cascade constraints;
drop table REACTION cascade constraints;
drop table QUESTION_OFFER cascade constraints;

drop table AVAILABILITY cascade constraints;
drop table SKILL cascade constraints;
drop table QUESTION_SKILL cascade constraints;
drop table PERSON_VOLUNTEER_SKILL cascade constraints;


CREATE TABLE FILES (
  ID          NUMBER        PRIMARY KEY,
  UPLOAD_DATE DATE          NOT NULL,
  CONTENT     BLOB          NOT NULL,
  TYPE        VARCHAR2(64)  NOT NULL,
  NAME        VARCHAR2(128) NOT NULL,
  LENGTH      NUMBER        NOT NULL
);

CREATE TABLE PERSON (
	Email				      varchar2(45) primary key,
	Password			    varchar2(45) NOT NULL,
	User_kind			    varchar2(45) CHECK(UPPER ( User_kind ) IN ( 'HELPSEEKER' , 'ADMIN' , 'VOLUNTEER' )),
	Name				      varchar2(45),
	Bio					      varchar2(400),
	Profile_image		  blob,
	Street_name			  varchar2(45),
	House_number		  varchar2(4),
	Zip_code			    varchar2(10),
	City				      varchar2(45),
	Date_of_birth		  date,
	Has_time_out		  number(1) DEFAULT 0 CHECK(Has_time_out IN ( 0 , 1 )),
	Time_out_duration	date,
  File_id           number,
  
  CONSTRAINT FK_PERSON_FILE_ID FOREIGN KEY (file_id) REFERENCES FILES(ID) on delete cascade
);

CREATE TABLE PERSON_VOLUNTEER 
(
	PERSON_EMAIL        varchar2(45) primary key,
	VOG_accepted		    number(1) DEFAULT 0 CHECK(VOG_accepted IN ( 0 , 1 )),
	Has_driving_license	number(1) DEFAULT 0 CHECK(Has_driving_license IN ( 0 , 1 )),
	Has_car	            number(1) DEFAULT 0 CHECK(Has_car IN ( 0 , 1 )),
  vog_id              number,
  
  foreign key(PERSON_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade ,
  foreign key (file_id) REFERENCES FILES(ID) on delete cascade
);  

CREATE TABLE PERSON_HELP_SEEKER 
( 
	PERSON_EMAIL                varchar2(45) primary key,
  Can_use_public_transport    number(1) DEFAULT 0  CHECK (Can_use_public_transport IN ( 0 , 1 )), 
 
  constraint FK_PHS_PERSON_EMAIL foreign key(PERSON_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade 
);

CREATE TABLE AVAILABILITY 
(
	PERSON_EMAIL        varchar2(45)  not null,
  Day                 varchar2(45)  not null,
  Daypart             varchar2(45)  not null,
    
  constraint FK_AVAILABILITY_PERSON_EMAIL foreign key(PERSON_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade,
  PRIMARY KEY(PERSON_EMAIL, Day)
);


--DROP SEQUENCE seq_question;
--CREATE SEQUENCE seq_question
--  MINVALUE 0
--  START WITH 0
--  INCREMENT BY 1;

CREATE TABLE QUESTION 
( 
    ID                number(8)           primary key, 
    HELPSEEKER_EMAIL  varchar2(45)        not null, 
    VOLUNTEER_EMAIL   varchar2(45)	      , 
    Title             varchar2(50)        not null, 
    Content           varchar2(4000)      not null, 
    Date_Time         date                , 
    Street_name       varchar2(45)        , 
    House_number      varchar2(4)         , 
    Zip_code          varchar2(10)        , 
    City              varchar2(45)        , 
    Status            varchar2(10)        not null,
    Transport         varchar2(45)        not null,
           
    constraint FK_QUESTION_HELPSEEKER_EMAIL foreign key(HELPSEEKER_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade,
    constraint FK_QUESTION_VOLUNTEER_EMAIL foreign key(VOLUNTEER_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade      
);


CREATE TABLE QUESTION_OFFER
(
  QUESTION_ID       number(8),
  VOLUNTEER_EMAIL  VARCHAR2(45),
  
  foreign key(QUESTION_ID) references QUESTION(id) on delete cascade,  
  foreign key(VOLUNTEER_EMAIL) references PERSON(email) on delete cascade,
  primary key(QUESTION_ID, VOLUNTEER_EMAIL)
);

--DROP SEQUENCE seq_review;
--CREATE SEQUENCE seq_review
--  MINVALUE 0
--  START WITH 0
--  INCREMENT BY 1;


CREATE TABLE REVIEW 
( 
    ID                number(8)         primary key, 
    HELPSEEKER_NAME  varchar2(45)      not null, 
    VOLUNTEER_EMAIL   varchar2(45)	    not null, 
    Content	          varchar2(400)     , 
    Rating            number(8,1)       , 
    Date_time         date              , 
     
    constraint FK_REVIEW_VOLUNTEER_ID foreign key(VOLUNTEER_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade
 );

SAVEPOINT TESTUSECASES;

DROP SEQUENCE seq_message;
CREATE SEQUENCE seq_message
  MINVALUE 0
  START WITH 0
  INCREMENT BY 1;

CREATE TABLE MESSAGE 
( 
<<<<<<< HEAD
	ID				number(8) 			primary key, 
    PERSON_NAME     varchar2(200)       not null, 
    CHAT_ID         number(8)           not null, 
    Content         varchar2(4000)      not null, 
    Date_Time       Date                not null, 
     
    constraint FK_MESSAGE_PERSON_NAME foreign key(PERSON_NAME)REFERENCES PERSON(NAME) on delete cascade, 
    constraint FK_MESSAGE_CHAT_ID foreign key(CHAT_ID)REFERENCES CHAT(ID) on delete cascade 
);


--DROP SEQUENCE seq_chat;
--CREATE SEQUENCE seq_chat
--  MINVALUE 0
--  START WITH 0
--  INCREMENT BY 1;


CREATE TABLE CHAT 
( 
    CHAT_ID              number(8)           primary key
);

CREATE TABLE PERSON_CHAT 
( 
    PERSON_EMAIL      VARCHAR2(255)       not null, 
    CHAT_ID           number(8)           not null, 
    TITLE             varchar2(255)       not null
     
    constraint FK_PERSON_CHAT_PERSON_EMAIL foreign key(PERSON_EMAIL)REFERENCES PERSON(EMAIL) on delete cascade, 
    constraint FK_PERSON_CHAT_CHAT_ID foreign key(CHAT_ID)REFERENCES CHAT(ID) on delete cascade, 
    constraint PK_PERSON_CHAT PRIMARY KEY (PERSON_ID, CHAT_ID) 
);


--DROP SEQUENCE seq_skill;
--CREATE SEQUENCE seq_skill
--  MINVALUE 0
--  START WITH 0
--  INCREMENT BY 1;

CREATE TABLE SKILL
(
    ID              number(8)           primary key,
    Description     varchar2(100)       not null
);

CREATE TABLE QUESTION_SKILL
(
    QUESTION_ID     number(8)           not null,
    SKILL_ID        number(8)           not null, 
    
    constraint FK_QUESTION_SKILL_QUESTION_ID foreign key(QUESTION_ID)REFERENCES QUESTION(ID) on delete cascade,  
    constraint FK_QUESTION_SKILL_SKILL_ID foreign key(SKILL_ID)REFERENCES SKILL(ID) on delete cascade, 
    constraint PK_QUESTION_SKILL PRIMARY KEY (QUESTION_ID, SKILL_ID )
);

CREATE TABLE PERSON_VOLUNTEER_SKILL
(
    VOLUNTEER_EMAIL  varchar2(45)        not null,
    SKILL_ID         number(8)           not null, 
    
    constraint FK_P_V_S_EMAIL foreign key(VOLUNTEER_EMAIL)REFERENCES PERSON_VOLUNTEER(PERSON_EMAIL) on delete cascade,  
    constraint FK_P_V_S_SKILL_ID foreign key(SKILL_ID)REFERENCES SKILL(ID) on delete cascade,
    constraint PK_P_V_S PRIMARY KEY (VOLUNTEER_EMAIL, SKILL_ID )
);


CREATE OR REPLACE PROCEDURE edit_planning(
  v_email IN VARCHAR2,
  v_mo    IN VARCHAR2,
  v_tu    IN VARCHAR2,
  v_we    IN VARCHAR2,
  v_th    IN VARCHAR2,
  v_fr    IN VARCHAR2,
  v_sa    IN VARCHAR2,
  v_su    IN VARCHAR2 )  
IS  
  v_count NUMBER;
BEGIN

  SELECT COUNT(*) INTO v_count  
  FROM AVAILABILITY
  WHERE person_email = v_email;
  
  -- if user already in table
  -- remove all record first
  IF v_count > 0 THEN
    BEGIN
      DELETE FROM AVAILABILITY WHERE person_email = v_email;
    END;
  END IF; 
  
  -- add new records for each day of the week
  BEGIN
    INSERT INTO AVAILABILITY VALUES ( v_email, 'mo', v_mo );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'tu', v_tu );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'we', v_we );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'th', v_th );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'fr', v_fr );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'sa', v_sa );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'su', v_su );  
  END;  
END;
/

-- this procedure makes it easy to import any BLOB files 
-- into the FILEStable
CREATE OR REPLACE PROCEDURE IMPORT_FILE(
  v_upload_date IN DATE,
  v_content     IN FILES.CONTENT%type,
  v_type        IN VARCHAR2,
  v_name        IN VARCHAR2,
  v_length      IN NUMBER,
  v_email       IN PROFILE.EMAIL%TYPE,
  v_id          OUT NUMBER)
IS
BEGIN
  SELECT files_seq.NEXTVAL INTO v_id FROM DUAL;
  
  INSERT INTO FILES 
  VALUES( v_id, v_upload_date, v_content, v_type, v_name, v_length );
  
  UPDATE PROFILE
    SET FILES_ID = v_id
    WHERE EMAIL = v_email;
    
END IMPORT_FILE;
/










