--------------------------------------------------------
--  File created - woensdag-januari-13-2016   
--------------------------------------------------------
DROP TABLE "DBI335167"."AVAILABILITY" cascade constraints;
DROP TABLE "DBI335167"."CHAT" cascade constraints;
DROP TABLE "DBI335167"."FILES" cascade constraints;
DROP TABLE "DBI335167"."MESSAGE" cascade constraints;
DROP TABLE "DBI335167"."MODULE" cascade constraints;
DROP TABLE "DBI335167"."PERSON" cascade constraints;
DROP TABLE "DBI335167"."PERSON_CHAT" cascade constraints;
DROP TABLE "DBI335167"."PERSON_HELP_SEEKER" cascade constraints;
DROP TABLE "DBI335167"."PERSON_VOLUNTEER" cascade constraints;
DROP TABLE "DBI335167"."QUESTION" cascade constraints;
DROP TABLE "DBI335167"."QUESTION_OFFER" cascade constraints;
DROP TABLE "DBI335167"."QUESTION_SKILL" cascade constraints;
DROP TABLE "DBI335167"."REACTION" cascade constraints;
DROP TABLE "DBI335167"."RESULTAAT" cascade constraints;
DROP TABLE "DBI335167"."REVIEW" cascade constraints;
DROP TABLE "DBI335167"."SKILL" cascade constraints;
DROP TABLE "DBI335167"."STUDENT" cascade constraints;
DROP SEQUENCE "DBI335167"."FILES_SEQ";
DROP SEQUENCE "DBI335167"."SEQ_CHAT";
DROP SEQUENCE "DBI335167"."SEQ_ID";
DROP SEQUENCE "DBI335167"."SEQ_MESSAGE";
DROP SEQUENCE "DBI335167"."SEQ_QUESTION";
DROP SEQUENCE "DBI335167"."SEQ_REVIEW";
DROP SEQUENCE "DBI335167"."SEQ_SKILL";
DROP PROCEDURE "DBI335167"."EDIT_PLANNING";
DROP PROCEDURE "DBI335167"."IMPORT_FILE";
DROP PROCEDURE "DBI335167"."IMPORT_VOG";
--------------------------------------------------------
--  DDL for Sequence FILES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."FILES_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 120 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_CHAT
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_CHAT"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 20 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_ID
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_MESSAGE
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_MESSAGE"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 40 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_QUESTION
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_QUESTION"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 80 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_REVIEW
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_REVIEW"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 20 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_SKILL
--------------------------------------------------------

   CREATE SEQUENCE  "DBI335167"."SEQ_SKILL"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 0 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AVAILABILITY
--------------------------------------------------------

  CREATE TABLE "DBI335167"."AVAILABILITY" 
   (	"PERSON_EMAIL" VARCHAR2(45 BYTE), 
	"DAY" VARCHAR2(45 BYTE), 
	"DAYPART" VARCHAR2(45 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table CHAT
--------------------------------------------------------

  CREATE TABLE "DBI335167"."CHAT" 
   (	"CHAT_ID" NUMBER(8,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table FILES
--------------------------------------------------------

  CREATE TABLE "DBI335167"."FILES" 
   (	"ID" NUMBER, 
	"UPLOAD_DATE" DATE, 
	"CONTENT" BLOB, 
	"TYPE" VARCHAR2(64 BYTE), 
	"NAME" VARCHAR2(128 BYTE), 
	"LENGTH" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("CONTENT") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table MESSAGE
--------------------------------------------------------

  CREATE TABLE "DBI335167"."MESSAGE" 
   (	"MESSAGE_ID" NUMBER(8,0), 
	"PERSON_NAME" VARCHAR2(255 BYTE), 
	"CHAT_ID" NUMBER(8,0), 
	"CONTENT" VARCHAR2(4000 BYTE), 
	"DATE_TIME" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table MODULE
--------------------------------------------------------

  CREATE TABLE "DBI335167"."MODULE" 
   (	"CODE" VARCHAR2(6 BYTE), 
	"OMSCHR" VARCHAR2(25 BYTE), 
	"FASE" VARCHAR2(1 BYTE), 
	"DOCENT" VARCHAR2(3 BYTE), 
	"SP" NUMBER(1,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PERSON
--------------------------------------------------------

  CREATE TABLE "DBI335167"."PERSON" 
   (	"EMAIL" VARCHAR2(45 BYTE), 
	"PASSWORD" VARCHAR2(45 BYTE), 
	"USER_KIND" VARCHAR2(45 BYTE), 
	"NAME" VARCHAR2(45 BYTE), 
	"BIO" VARCHAR2(400 BYTE), 
	"PROFILE_IMAGE" BLOB, 
	"STREET_NAME" VARCHAR2(45 BYTE), 
	"HOUSE_NUMBER" VARCHAR2(4 BYTE), 
	"ZIP_CODE" VARCHAR2(10 BYTE), 
	"CITY" VARCHAR2(45 BYTE), 
	"DATE_OF_BIRTH" DATE, 
	"HAS_TIME_OUT" NUMBER(1,0) DEFAULT 0, 
	"TIME_OUT_DURATION" DATE, 
	"FILE_ID" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("PROFILE_IMAGE") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table PERSON_CHAT
--------------------------------------------------------

  CREATE TABLE "DBI335167"."PERSON_CHAT" 
   (	"PERSON_EMAIL" VARCHAR2(255 BYTE), 
	"CHAT_ID" NUMBER(8,0), 
	"TITLE" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PERSON_HELP_SEEKER
--------------------------------------------------------

  CREATE TABLE "DBI335167"."PERSON_HELP_SEEKER" 
   (	"PERSON_EMAIL" VARCHAR2(45 BYTE), 
	"CAN_USE_PUBLIC_TRANSPORT" NUMBER(1,0) DEFAULT 0
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PERSON_VOLUNTEER
--------------------------------------------------------

  CREATE TABLE "DBI335167"."PERSON_VOLUNTEER" 
   (	"PERSON_EMAIL" VARCHAR2(45 BYTE), 
	"VOG_ACCEPTED" NUMBER(1,0) DEFAULT 0, 
	"HAS_DRIVING_LICENSE" NUMBER(1,0) DEFAULT 0, 
	"HAS_CAR" NUMBER(1,0) DEFAULT 0, 
	"VOG_ID" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table QUESTION
--------------------------------------------------------

  CREATE TABLE "DBI335167"."QUESTION" 
   (	"ID" NUMBER(8,0), 
	"HELPSEEKER_EMAIL" VARCHAR2(45 BYTE), 
	"VOLUNTEER_EMAIL" VARCHAR2(45 BYTE), 
	"TITLE" VARCHAR2(50 BYTE), 
	"CONTENT" VARCHAR2(4000 BYTE), 
	"DATE_TIME" DATE, 
	"STREET_NAME" VARCHAR2(45 BYTE), 
	"HOUSE_NUMBER" VARCHAR2(4 BYTE), 
	"ZIP_CODE" VARCHAR2(10 BYTE), 
	"CITY" VARCHAR2(45 BYTE), 
	"STATUS" VARCHAR2(10 BYTE), 
	"TRANSPORT" VARCHAR2(45 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table QUESTION_OFFER
--------------------------------------------------------

  CREATE TABLE "DBI335167"."QUESTION_OFFER" 
   (	"QUESTION_ID" NUMBER(8,0), 
	"VOLUNTEER_EMAIL" VARCHAR2(45 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table QUESTION_SKILL
--------------------------------------------------------

  CREATE TABLE "DBI335167"."QUESTION_SKILL" 
   (	"QUESTION_ID" NUMBER(8,0), 
	"SKILL_ID" NUMBER(8,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table REACTION
--------------------------------------------------------

  CREATE TABLE "DBI335167"."REACTION" 
   (	"PERSON_EMAIL" VARCHAR2(45 BYTE), 
	"QUESTION_ID" NUMBER(8,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RESULTAAT
--------------------------------------------------------

  CREATE TABLE "DBI335167"."RESULTAAT" 
   (	"CODE" VARCHAR2(6 BYTE), 
	"STUDNR" NUMBER(8,0), 
	"DATUM" DATE, 
	"CIJFER" NUMBER(2,0), 
	"DOCENT" VARCHAR2(3 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table REVIEW
--------------------------------------------------------

  CREATE TABLE "DBI335167"."REVIEW" 
   (	"ID" NUMBER(8,0), 
	"HELPSEEKER_NAME" VARCHAR2(45 BYTE), 
	"VOLUNTEER_EMAIL" VARCHAR2(45 BYTE), 
	"CONTENT" VARCHAR2(400 BYTE), 
	"RATING" NUMBER(8,1), 
	"DATE_TIME" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SKILL
--------------------------------------------------------

  CREATE TABLE "DBI335167"."SKILL" 
   (	"ID" NUMBER(8,0), 
	"DESCRIPTION" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table STUDENT
--------------------------------------------------------

  CREATE TABLE "DBI335167"."STUDENT" 
   (	"STUDNR" NUMBER(8,0), 
	"NAAM" VARCHAR2(25 BYTE), 
	"GEBDAT" DATE, 
	"GESLACHT" VARCHAR2(1 BYTE), 
	"VOOROPLEIDING" VARCHAR2(4 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
<<<<<<< HEAD
REM INSERTING into DBI335167.AVAILABILITY
SET DEFINE OFF;
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','mo','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','tu','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','we','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','th','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','fr','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','sa','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user5@gmail.com','su','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','mo','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','tu','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','we','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','th','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','fr','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','sa','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('basvanzutphen@gmail.com','su','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','mo','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','tu','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','we','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','th','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','fr','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','sa','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('volunteer@gmail.com','su','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','mo','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','tu','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','we','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','th','''s Ochtends');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','fr','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','sa','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user20@gmail.com','su','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','mo','s'' Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','tu','s'' Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','we','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','th','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','fr','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','sa','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('joeyheester@gmail.com','su','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','mo','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','tu','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','we','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','th','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','fr','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','sa','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('user1@gmail.com','su','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','mo','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','tu','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','we','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','th','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','fr','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','sa','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('hugomka@outlook.com','su','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','mo','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','tu','Hele dag');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','we','''s Middags');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','th','''s Avonds');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','fr','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','sa','-');
Insert into DBI335167.AVAILABILITY (PERSON_EMAIL,DAY,DAYPART) values ('jimvercoelen@gmail.com','su','-');
REM INSERTING into DBI335167.CHAT
SET DEFINE OFF;
Insert into DBI335167.CHAT (CHAT_ID) values ('2');
Insert into DBI335167.CHAT (CHAT_ID) values ('7');
Insert into DBI335167.CHAT (CHAT_ID) values ('8');
REM INSERTING into DBI335167.FILES
SET DEFINE OFF;
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('85',to_date('01/12/2016 15:25:06','MM/DD/YYYY HH24:MI:SS'),'image/png','bas.PNG','266670');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('86',to_date('01/12/2016 19:33:58','MM/DD/YYYY HH24:MI:SS'),'image/png','oma.PNG','1164602');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('102',to_date('01/13/2016 17:52:33','MM/DD/YYYY HH24:MI:SS'),'image/png','bas.PNG','266670');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('64',to_date('01/09/2016 03:17:20','MM/DD/YYYY HH24:MI:SS'),'image/gif','banana.gif','73623');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('61',to_date('01/09/2016 03:04:06','MM/DD/YYYY HH24:MI:SS'),'image/png','noimage.png','1020');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('63',to_date('01/09/2016 03:09:16','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','Ad van Baarle.jpg','22572');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('82',to_date('01/11/2016 23:06:23','MM/DD/YYYY HH24:MI:SS'),'image/png','opa1.PNG','718407');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('100',to_date('01/13/2016 15:55:39','MM/DD/YYYY HH24:MI:SS'),'image/gif','banana.gif','73623');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('101',to_date('01/13/2016 16:19:59','MM/DD/YYYY HH24:MI:SS'),'application/pdf','voorbeeld-vog-2015_tcm123-529365.pdf','382240');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('105',to_date('01/13/2016 18:47:43','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','oma2.jpg','83477');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('67',to_date('01/10/2016 20:35:05','MM/DD/YYYY HH24:MI:SS'),'image/png','profiel foto.PNG','290466');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('65',to_date('01/09/2016 03:27:05','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','boos.jpg','68125');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('66',to_date('01/09/2016 03:28:52','MM/DD/YYYY HH24:MI:SS'),'image/gif','banana.gif','73623');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('68',to_date('01/10/2016 21:31:27','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','img-03.jpg','24499');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('69',to_date('01/10/2016 21:32:04','MM/DD/YYYY HH24:MI:SS'),'image/png','profiel foto.PNG','290466');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('70',to_date('01/10/2016 21:32:29','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','img-03.jpg','24499');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('71',to_date('01/10/2016 21:32:34','MM/DD/YYYY HH24:MI:SS'),'image/png','profiel foto.PNG','290466');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('72',to_date('01/10/2016 21:33:13','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','img-03.jpg','24499');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('73',to_date('01/10/2016 21:33:27','MM/DD/YYYY HH24:MI:SS'),'image/png','profiel foto.PNG','290466');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('74',to_date('01/10/2016 21:44:32','MM/DD/YYYY HH24:MI:SS'),'image/png','profiel foto.PNG','290466');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('75',to_date('01/11/2016 04:35:02','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','1.jpg','96749');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('76',to_date('01/11/2016 16:04:17','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','4.jpg','188916');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('77',to_date('01/11/2016 22:54:49','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','2.jpg','79325');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('78',to_date('01/11/2016 22:58:10','MM/DD/YYYY HH24:MI:SS'),'image/png','hugo.PNG','391099');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('79',to_date('01/11/2016 22:59:10','MM/DD/YYYY HH24:MI:SS'),'image/png','bas.PNG','266670');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('80',to_date('01/11/2016 23:00:16','MM/DD/YYYY HH24:MI:SS'),'image/png','oma1.PNG','877683');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('103',to_date('01/13/2016 18:38:32','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','oma2.jpg','83477');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('104',to_date('01/13/2016 18:39:40','MM/DD/YYYY HH24:MI:SS'),'image/jpeg','3.jpg','221409');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('81',to_date('01/11/2016 23:02:45','MM/DD/YYYY HH24:MI:SS'),'image/png','joey.PNG','849951');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('83',to_date('01/11/2016 23:09:11','MM/DD/YYYY HH24:MI:SS'),'image/png','opa2.PNG','553099');
Insert into DBI335167.FILES (ID,UPLOAD_DATE,TYPE,NAME,LENGTH) values ('84',to_date('01/12/2016 15:21:57','MM/DD/YYYY HH24:MI:SS'),'image/png','hugo.PNG','391099');
REM INSERTING into DBI335167.MESSAGE
SET DEFINE OFF;
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('14','gewijzigd','7','Test',to_date('01/13/2016 01:07:51','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('5','gewijzigd','7','Test',to_date('01/13/2016 01:05:05','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('8','gewijzigd','7','Test',to_date('01/13/2016 01:06:14','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('9','gewijzigd','7','Test',to_date('01/13/2016 01:06:14','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('10','gewijzigd','7','Test',to_date('01/13/2016 01:06:18','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('16','gewijzigd','2','Test2',to_date('01/13/2016 01:06:33','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('4','gewijzigd','7','Test',to_date('01/13/2016 01:01:41','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('6','gewijzigd','7','Test',to_date('01/13/2016 01:05:32','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('7','gewijzigd','7','Test',to_date('01/13/2016 01:06:12','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('11','gewijzigd','7','Test',to_date('01/13/2016 01:06:18','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('12','gewijzigd','7','Test',to_date('01/13/2016 01:06:31','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('13','gewijzigd','7','Test',to_date('01/13/2016 01:06:33','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('15','Kees van leeuwen','7','Test',to_date('01/13/2016 01:10:33','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('21','Francis Loos','8','Goede middag ',to_date('01/13/2016 18:57:22','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('22','Oma 2','8','Hallo',to_date('01/13/2016 18:58:07','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('17','gewijzigd','2','test terug',to_date('01/13/2016 17:43:42','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('18','gewijzigd','2','test terug 2',to_date('01/13/2016 17:44:50','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('19','gewijzigd','7','je oma',to_date('01/13/2016 17:45:13','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.MESSAGE (MESSAGE_ID,PERSON_NAME,CHAT_ID,CONTENT,DATE_TIME) values ('20','gewijzigd','2','test 3333',to_date('01/13/2016 17:45:45','MM/DD/YYYY HH24:MI:SS'));
REM INSERTING into DBI335167.MODULE
SET DEFINE OFF;
Insert into DBI335167.MODULE (CODE,OMSCHR,FASE,DOCENT,SP) values ('OIS','Software','P','PI','8');
Insert into DBI335167.MODULE (CODE,OMSCHR,FASE,DOCENT,SP) values ('OIT','Technology','P','PA','7');
Insert into DBI335167.MODULE (CODE,OMSCHR,FASE,DOCENT,SP) values ('OIM','Media','P','MA','8');
Insert into DBI335167.MODULE (CODE,OMSCHR,FASE,DOCENT,SP) values ('OIB','Business','P','BA','6');
REM INSERTING into DBI335167.PERSON
SET DEFINE OFF;
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('jimvercoelen@gmail.com','Wachtwoord542354','Volunteer','Jim Vercoelen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget massa quis ligula viverra ornare. Vivamus accumsan justo ut libero volutpat.',null,null,null,'Utrecht',to_date('12/08/1993 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'74');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user20@gmail.com','Wachtwoord542354','Volunteer','Sophie van de Vliet','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec libero molestie, iaculis diam vitae, commodo sapien. Nunc ut tellus non purus placerat mattis.',null,null,null,'Reuzel',to_date('08/29/1991 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'77');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('helpseeker@gmail.com','Wachtwoord542354','Helpseeker','Oma 2','Morbi lacinia consequat aliquet. Phasellus eu lacinia lacus, ac accumsan mi. In nec auctor sem. Curabitur hendrerit, tortor id dictum aliquet, nu',null,null,null,'Ergens',to_date('01/09/1976 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'105');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('hugomka@outlook.com','Wachtwoord542354','Volunteer','Hugo Mkandawire','Vrijwilliger',null,null,null,'''s-Hertogenbosch',to_date('04/06/1985 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'85');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('basvanzutphen@gmail.com','Wachtwoord542354','Volunteer','gewijzigd','Ik ben Bas en ik houd van buitenspelen',null,null,null,'Veghel',to_date('01/22/2016 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'102');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user3@gmail.com','Wachtwoord542354','Helpseeker','Elsa Klaassen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec libero molestie, iaculis diam vitae, commodo sapien. Nunc ut tellus non purus placerat mattis.',null,null,null,'Luykgestel',to_date('03/11/1946 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'80');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('annroijackers@gmail.com','Wachtwoord542354','Helpseeker','Ann','Ik ben Ann',null,null,null,'Luyksgestel',to_date('05/15/1949 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',to_date('01/12/2016 00:00:00','MM/DD/YYYY HH24:MI:SS'),'86');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user5@gmail.com','Wachtwoord542354','Volunteer','Karel Etterkroon','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tellus metus, ullamcorper quis elementum et, finibus eu massa. Suspendisse potenti.',null,null,null,'Amsterdam',to_date('03/07/1982 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'75');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('joeyheester@gmail.com','Wachtwoord542354','Volunteer','Joey Heesters','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quam nunc, faucibus nec nunc eu, dictum consequat massa. Proin lacus nulla, iaculis lacinia convallis ut, lobortis sit amet lectus. Ut at cursus orci. ',null,null,null,'Bergeijk',to_date('11/08/1993 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'81');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('admin@gmail.com','Wachtwoord542354','ADMIN',null,null,null,null,null,null,null,'0',null,'61');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('hugomka@gmail.com','Wachtwoord542354','Helpseeker','Hugo Mkandawire','Hulpbehoevende',null,null,null,'''s-Hertogenbosch',to_date('04/06/1985 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'61');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user4@gmail.com','Wachtwoord542354','Helpseeker','Bertus Antonis','Donec ultrices eros turpis, ut ultrices sem varius a. Etiam porta ut nisl ut imperdiet. Maecenas at augue eu tellus pulvinar condimentum et nec nunc.',null,null,null,'Bergeijk',to_date('05/01/1947 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'82');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('volunteer@gmail.com','Wachtwoord542354','Volunteer','Francis Loos','Morbi lacinia consequat aliquet. Phasellus eu lacinia lacus, ac accumsan mi. In nec auctor sem. Curabitur hendrerit, tortor id dictum aliquet, nu',null,null,null,'Ergens',to_date('01/09/1993 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'104');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user1@gmail.com','Wachtwoord542354','Volunteer','Veerle Martens','Sed vitae nunc ac purus porttitor aliquam nec non elit. Morbi vehicula luctus sem. Duis varius ante ac semper vehicula.',null,null,null,'Eindhoven',to_date('01/08/1991 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'76');
Insert into DBI335167.PERSON (EMAIL,PASSWORD,USER_KIND,NAME,BIO,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,DATE_OF_BIRTH,HAS_TIME_OUT,TIME_OUT_DURATION,FILE_ID) values ('user2@gmail.com','Wachtwoord542354','Helpseeker','Kees van leeuwen','Lorem ipsum dolor sit amet, consectetur adipiscing elit.',null,null,null,'Eersel',to_date('08/12/1934 00:00:00','MM/DD/YYYY HH24:MI:SS'),'0',null,'83');
REM INSERTING into DBI335167.PERSON_CHAT
SET DEFINE OFF;
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('helpseeker@gmail.com','8','Francis Loos');
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('basvanzutphen@gmail.com','7','Kees van leeuwen');
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('user2@gmail.com','7','Bas van Zutphen');
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('volunteer@gmail.com','8','Oma 2');
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('basvanzutphen@gmail.com','2','Elsa Klaassen');
Insert into DBI335167.PERSON_CHAT (PERSON_EMAIL,CHAT_ID,TITLE) values ('user3@gmail.com','2','Bas van Zutphen');
REM INSERTING into DBI335167.PERSON_HELP_SEEKER
SET DEFINE OFF;
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('helpseeker@gmail.com','1');
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('user3@gmail.com','1');
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('annroijackers@gmail.com','1');
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('hugomka@gmail.com','1');
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('user4@gmail.com','1');
Insert into DBI335167.PERSON_HELP_SEEKER (PERSON_EMAIL,CAN_USE_PUBLIC_TRANSPORT) values ('user2@gmail.com','1');
REM INSERTING into DBI335167.PERSON_VOLUNTEER
SET DEFINE OFF;
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('jimvercoelen@gmail.com','0','0','1',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('user20@gmail.com','0','1','1',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('hugomka@outlook.com','0','1','1',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('basvanzutphen@gmail.com','0','1','0','101');
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('user5@gmail.com','0','1','1',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('joeyheester@gmail.com','0','1','0',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('volunteer@gmail.com','0','1','1',null);
Insert into DBI335167.PERSON_VOLUNTEER (PERSON_EMAIL,VOG_ACCEPTED,HAS_DRIVING_LICENSE,HAS_CAR,VOG_ID) values ('user1@gmail.com','0','1','1',null);
REM INSERTING into DBI335167.QUESTION
SET DEFINE OFF;
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('72','annroijackers@gmail.com','jimvercoelen@gmail.com','Testvraag 2','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hendrerit enim et porta consequat. Vestibulum nisl arcu, commodo ultricies consequat euismod, aliquam vitae quam. Aliquam posuere enim id tortor iaculis, et ultrices quam ornare. Duis accumsan purus a tempus condimentum. Nullam in enim diam. Fusce id magna eros. Vivamus consequat nisl sit amet est tincidunt, vel tempor lectus ornare. Etiam condimentum, nisi nec porttitor commodo, dolor lectus vehicula diam, quis porttitor tortor felis eget sapien. Sed sed laoreet mauris, at finibus purus.',to_date('01/12/2016 22:46:14','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luyksgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('74','annroijackers@gmail.com','jimvercoelen@gmail.com','Testvraag 4','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hendrerit enim et porta consequat. Vestibulum nisl arcu, commodo ultricies consequat euismod, aliquam vitae quam. Aliquam posuere enim id tortor iaculis, et ultrices quam ornare. Duis accumsan purus a tempus condimentum. Nullam in enim diam. Fusce id magna eros. Vivamus consequat nisl sit amet est tincidunt, vel tempor lectus ornare. Etiam condimentum, nisi nec porttitor commodo, dolor lectus vehicula diam, quis porttitor tortor felis eget sapien. Sed sed laoreet mauris, at finibus purus.',to_date('01/12/2016 22:46:32','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luyksgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('75','user3@gmail.com','basvanzutphen@gmail.com','ChatTest3','Chattest3 chattest 3 chattest3 chattest3 chattest3 chattestdrieeeehiee',to_date('01/12/2016 22:50:24','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luykgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('76','user2@gmail.com','basvanzutphen@gmail.com','Chattest4','rtertretertertetert t rtyer yerty erty erty erty erty erty erty',to_date('01/13/2016 00:10:58','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Eersel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('6','user2@gmail.com','basvanzutphen@gmail.com','Wie kan er mee boodschappen doen?','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla dignissim nunc, eu lobortis dolor luctus vitae. Sed ultrices, lacus in porta gravida,',to_date('01/04/2016 19:50:25','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Jumbo Luykgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('10','user2@gmail.com','basvanzutphen@gmail.com','Ik heb hulp nodig bij belasting aangifte','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla dignissim nunc, eu lobortis dolor luctus vitae. Sed ultrices, lacus in porta gravida, tellus orci bibendum neque, at dapibus massa massa vitae erat. Donec nulla orci, auctor a sagittis sit amet, vestibulum vitae nibh. Nam tincidunt vehicula aliquet.',to_date('01/04/2016 21:35:57','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Eersel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('8','user2@gmail.com','user1@gmail.com','Kan iemand mee beer uitlaten?!','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla dignissim nunc, eu lobortis dolor luctus vitae. Sed ultrices, lacus in porta gravida, tellus orci bibendum neque, at dapibus massa massa vitae erat. Donec nulla orci, auctor a sagittis sit amet, vestibulum vitae nibh.',to_date('01/04/2016 21:33:28','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Thuis adres','INPROGRESS','Te voet');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('62','user4@gmail.com',null,'Ik kan de ramen niet lappen','Etiam non ex at purus fringilla sodales eget a sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam sodales nulla in turpis suscipit, quis vestibulum massa dignissim. Donec justo leo, varius in placerat in, maximus convallis nibh. Ut aliquam elit in mattis egestas. Fusce lectus nibh, varius vitae auctor vestibulum, imperdiet sed lectus. Curabitur in ipsum nulla. Donec ac mollis tellus, eu bibendum erat. Aliquam erat volutpat.',to_date('01/11/2016 23:07:30','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Bergeijk','OPEN','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('69','user3@gmail.com','basvanzutphen@gmail.com','Dit is nog een chattest','Wederom een chattest pls halp thankx',to_date('01/12/2016 21:54:20','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luykgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('77','annroijackers@gmail.com','user1@gmail.com','testvraag 5','testtesttesttesttesttesttesttest testtesttesttesttesttesttest testtesttesttesttesttesttesttest testtesttesttesttest testtesttesttest testtesttesttesttest testtesttesttesttest',to_date('01/13/2016 01:01:16','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luyksgestel','INPROGRESS','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('63','user4@gmail.com',null,'Wie kan mijn hondje mee uitlaten?','Etiam non ex at purus fringilla sodales eget a sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam sodales nulla in turpis suscipit, quis vestibulum massa dignissim. Donec justo leo, varius in placerat in, maximus convallis nibh. Ut aliquam elit in mattis egestas. Fusce lectus nibh, varius vitae auctor vestibulum, imperdiet sed lectus. Curabitur in ipsum nulla. Donec ac mollis tellus, eu bibendum erat. Aliquam erat volutpat.',to_date('01/11/2016 23:08:01','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Bergeijk centraal parkje','OPEN','Te voet');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('60','user3@gmail.com',null,'Ik heb gezelschap nodig','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec libero molestie, iaculis diam vitae, commodo sapien. Nunc ut tellus non purus placerat mattis. Donec ultrices eros turpis, ut ultrices sem varius a. Etiam porta ut nisl ut imperdiet. Maecenas at augue eu tellus pulvinar condimentum et nec nunc. Ut ultrices felis diam, eget condimentum velit posuere non. Aenean pretium magna at urna consectetur, ac tincidunt mi euismod. Donec in purus in diam venenatis efficitur ut quis velit. Vestibulum euismod vel odio id laoreet. Sed id risus imperdiet, lacinia purus in, suscipit orci. Vivamus consequat placerat metus, vel viverra nunc. Nam sodales tristique arcu eu volutpat.',to_date('01/11/2016 23:01:27','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luykgestel','OPEN','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('61','user3@gmail.com',null,'Wie kan de dieren komen voederen?','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec libero molestie, iaculis diam vitae, commodo sapien. Nunc ut tellus non purus placerat mattis. Donec ultrices eros turpis, ut ultrices sem varius a. Etiam porta ut nisl ut imperdiet. Maecenas at augue eu tellus pulvinar condimentum et nec nunc. Ut ultrices felis diam, eget condimentum velit posuere non. Aenean pretium magna at urna consectetur, ac tincidunt mi euismod. Donec in purus in diam venenatis efficitur ut quis velit. Vestibulum euismod vel odio id laoreet. Sed id risus imperdiet, lacinia purus in, suscipit orci. Vivamus consequat placerat metus, vel viverra nunc.',to_date('01/11/2016 23:01:48','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luykgestel','OPEN','n.v.t.');
Insert into DBI335167.QUESTION (ID,HELPSEEKER_EMAIL,VOLUNTEER_EMAIL,TITLE,CONTENT,DATE_TIME,STREET_NAME,HOUSE_NUMBER,ZIP_CODE,CITY,STATUS,TRANSPORT) values ('70','user3@gmail.com','basvanzutphen@gmail.com','Bas van zupthen chat test','Bas help mij groetjes Elsa Klaassen uit Luyksgestel',to_date('01/12/2016 22:43:00','MM/DD/YYYY HH24:MI:SS'),null,null,null,'Luykgestel','INPROGRESS','n.v.t.');
REM INSERTING into DBI335167.QUESTION_OFFER
SET DEFINE OFF;
Insert into DBI335167.QUESTION_OFFER (QUESTION_ID,VOLUNTEER_EMAIL) values ('60','jimvercoelen@gmail.com');
Insert into DBI335167.QUESTION_OFFER (QUESTION_ID,VOLUNTEER_EMAIL) values ('61','joeyheester@gmail.com');
Insert into DBI335167.QUESTION_OFFER (QUESTION_ID,VOLUNTEER_EMAIL) values ('63','volunteer@gmail.com');
REM INSERTING into DBI335167.QUESTION_SKILL
SET DEFINE OFF;
REM INSERTING into DBI335167.REACTION
SET DEFINE OFF;
REM INSERTING into DBI335167.RESULTAAT
SET DEFINE OFF;
Insert into DBI335167.RESULTAAT (CODE,STUDNR,DATUM,CIJFER,DOCENT) values ('OIS','200',to_date('05/15/2015 00:00:00','MM/DD/YYYY HH24:MI:SS'),'8','Bas');
Insert into DBI335167.RESULTAAT (CODE,STUDNR,DATUM,CIJFER,DOCENT) values ('OIS','201',to_date('05/15/2015 00:00:00','MM/DD/YYYY HH24:MI:SS'),'9','Bas');
Insert into DBI335167.RESULTAAT (CODE,STUDNR,DATUM,CIJFER,DOCENT) values ('OIS','202',to_date('05/15/2015 00:00:00','MM/DD/YYYY HH24:MI:SS'),'9','Bas');
Insert into DBI335167.RESULTAAT (CODE,STUDNR,DATUM,CIJFER,DOCENT) values ('OIS','203',to_date('05/15/2015 00:00:00','MM/DD/YYYY HH24:MI:SS'),'2','Bas');
Insert into DBI335167.RESULTAAT (CODE,STUDNR,DATUM,CIJFER,DOCENT) values ('OIT','200',to_date('05/15/2015 00:00:00','MM/DD/YYYY HH24:MI:SS'),'10','Bas');
REM INSERTING into DBI335167.REVIEW
SET DEFINE OFF;
Insert into DBI335167.REVIEW (ID,HELPSEEKER_NAME,VOLUNTEER_EMAIL,CONTENT,RATING,DATE_TIME) values ('6','annroijackers@gmail.com','jimvercoelen@gmail.com','Wederom bedankt Jim!','4,5',to_date('01/13/2016 01:50:47','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.REVIEW (ID,HELPSEEKER_NAME,VOLUNTEER_EMAIL,CONTENT,RATING,DATE_TIME) values ('5','annroijackers@gmail.com','jimvercoelen@gmail.com','Jim heeft mij goed geholpen, dankjewel!','3,5',to_date('01/13/2016 01:42:17','MM/DD/YYYY HH24:MI:SS'));
Insert into DBI335167.REVIEW (ID,HELPSEEKER_NAME,VOLUNTEER_EMAIL,CONTENT,RATING,DATE_TIME) values ('7','Oma 2','volunteer@gmail.com','Goed gedaan dankje','4,5',to_date('01/13/2016 18:58:49','MM/DD/YYYY HH24:MI:SS'));
REM INSERTING into DBI335167.SKILL
SET DEFINE OFF;
REM INSERTING into DBI335167.STUDENT
SET DEFINE OFF;
Insert into DBI335167.STUDENT (STUDNR,NAAM,GEBDAT,GESLACHT,VOOROPLEIDING) values ('200','Daphne',to_date('01/01/1990 00:00:00','MM/DD/YYYY HH24:MI:SS'),'V','VWO');
Insert into DBI335167.STUDENT (STUDNR,NAAM,GEBDAT,GESLACHT,VOOROPLEIDING) values ('201','Jim',to_date('02/01/1992 00:00:00','MM/DD/YYYY HH24:MI:SS'),'M','MBO');
Insert into DBI335167.STUDENT (STUDNR,NAAM,GEBDAT,GESLACHT,VOOROPLEIDING) values ('202','Hugo',to_date('04/06/1985 00:00:00','MM/DD/YYYY HH24:MI:SS'),'M','MBO');
Insert into DBI335167.STUDENT (STUDNR,NAAM,GEBDAT,GESLACHT,VOOROPLEIDING) values ('203','Demian',to_date('03/01/1999 00:00:00','MM/DD/YYYY HH24:MI:SS'),'M','HAVO');
Insert into DBI335167.STUDENT (STUDNR,NAAM,GEBDAT,GESLACHT,VOOROPLEIDING) values ('204','Bas',to_date('05/01/1998 00:00:00','MM/DD/YYYY HH24:MI:SS'),'M','HAVO');
=======

>>>>>>> cf7abb73ff293466811665f25e3888a3113593ba
--------------------------------------------------------
--  DDL for Index PK_QUESTION_SKILL
--------------------------------------------------------

  CREATE UNIQUE INDEX "DBI335167"."PK_QUESTION_SKILL" ON "DBI335167"."QUESTION_SKILL" ("QUESTION_ID", "SKILL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PK_PERSON_CHAT
--------------------------------------------------------

  CREATE UNIQUE INDEX "DBI335167"."PK_PERSON_CHAT" ON "DBI335167"."PERSON_CHAT" ("PERSON_EMAIL", "CHAT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PK_REACTION
--------------------------------------------------------

  CREATE UNIQUE INDEX "DBI335167"."PK_REACTION" ON "DBI335167"."REACTION" ("PERSON_EMAIL", "QUESTION_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table RESULTAAT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."RESULTAAT" ADD CONSTRAINT "CIJFERBETWEENONEANDTEN" CHECK (cijfer BETWEEN 1 AND 10) ENABLE;
 
  ALTER TABLE "DBI335167"."RESULTAAT" ADD PRIMARY KEY ("CODE", "STUDNR", "DATUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table QUESTION_OFFER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION_OFFER" ADD PRIMARY KEY ("QUESTION_ID", "VOLUNTEER_EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table SKILL
--------------------------------------------------------

  ALTER TABLE "DBI335167"."SKILL" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."SKILL" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table REVIEW
--------------------------------------------------------

  ALTER TABLE "DBI335167"."REVIEW" MODIFY ("HELPSEEKER_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."REVIEW" MODIFY ("VOLUNTEER_EMAIL" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."REVIEW" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table FILES
--------------------------------------------------------

  ALTER TABLE "DBI335167"."FILES" MODIFY ("UPLOAD_DATE" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."FILES" MODIFY ("CONTENT" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."FILES" MODIFY ("TYPE" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."FILES" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."FILES" MODIFY ("LENGTH" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."FILES" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table AVAILABILITY
--------------------------------------------------------

  ALTER TABLE "DBI335167"."AVAILABILITY" MODIFY ("PERSON_EMAIL" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."AVAILABILITY" MODIFY ("DAY" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."AVAILABILITY" MODIFY ("DAYPART" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."AVAILABILITY" ADD PRIMARY KEY ("PERSON_EMAIL", "DAY")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table QUESTION
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION" MODIFY ("HELPSEEKER_EMAIL" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION" MODIFY ("TITLE" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION" MODIFY ("CONTENT" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION" MODIFY ("STATUS" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION" MODIFY ("TRANSPORT" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table QUESTION_SKILL
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION_SKILL" ADD CONSTRAINT "PK_QUESTION_SKILL" PRIMARY KEY ("QUESTION_ID", "SKILL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DBI335167"."QUESTION_SKILL" MODIFY ("QUESTION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."QUESTION_SKILL" MODIFY ("SKILL_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PERSON_CHAT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_CHAT" ADD CONSTRAINT "PK_PERSON_CHAT" PRIMARY KEY ("PERSON_EMAIL", "CHAT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_CHAT" MODIFY ("PERSON_EMAIL" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."PERSON_CHAT" MODIFY ("CHAT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."PERSON_CHAT" MODIFY ("TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table STUDENT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."STUDENT" ADD PRIMARY KEY ("STUDNR")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table REACTION
--------------------------------------------------------

  ALTER TABLE "DBI335167"."REACTION" ADD CONSTRAINT "PK_REACTION" PRIMARY KEY ("PERSON_EMAIL", "QUESTION_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DBI335167"."REACTION" MODIFY ("PERSON_EMAIL" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."REACTION" MODIFY ("QUESTION_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PERSON_VOLUNTEER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_VOLUNTEER" ADD CHECK (VOG_accepted IN ( 0 , 1 )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_VOLUNTEER" ADD CHECK (Has_driving_license IN ( 0 , 1 )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_VOLUNTEER" ADD CHECK (Has_car IN ( 0 , 1 )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_VOLUNTEER" ADD PRIMARY KEY ("PERSON_EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table MESSAGE
--------------------------------------------------------

  ALTER TABLE "DBI335167"."MESSAGE" MODIFY ("PERSON_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."MESSAGE" MODIFY ("CHAT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."MESSAGE" MODIFY ("CONTENT" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."MESSAGE" MODIFY ("DATE_TIME" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."MESSAGE" ADD PRIMARY KEY ("MESSAGE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PERSON
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON" MODIFY ("PASSWORD" NOT NULL ENABLE);
 
  ALTER TABLE "DBI335167"."PERSON" ADD CHECK (UPPER ( User_kind ) IN ( 'HELPSEEKER' , 'ADMIN' , 'VOLUNTEER' )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON" ADD CHECK (Has_time_out IN ( 0 , 1 )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON" ADD PRIMARY KEY ("EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table CHAT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."CHAT" ADD PRIMARY KEY ("CHAT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PERSON_HELP_SEEKER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_HELP_SEEKER" ADD CHECK (Can_use_public_transport IN ( 0 , 1 )) ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_HELP_SEEKER" ADD PRIMARY KEY ("PERSON_EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table MODULE
--------------------------------------------------------

  ALTER TABLE "DBI335167"."MODULE" ADD PRIMARY KEY ("CODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AVAILABILITY
--------------------------------------------------------

  ALTER TABLE "DBI335167"."AVAILABILITY" ADD CONSTRAINT "FK_AVAILABILITY_PERSON_EMAIL" FOREIGN KEY ("PERSON_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MESSAGE
--------------------------------------------------------

  ALTER TABLE "DBI335167"."MESSAGE" ADD CONSTRAINT "FK_MESSAGE_CHAT_ID" FOREIGN KEY ("CHAT_ID")
	  REFERENCES "DBI335167"."CHAT" ("CHAT_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PERSON
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON" ADD CONSTRAINT "FK_PERSON_FILE_ID" FOREIGN KEY ("FILE_ID")
	  REFERENCES "DBI335167"."FILES" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PERSON_CHAT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_CHAT" ADD CONSTRAINT "FK_PERSON_CHAT_CHAT_ID" FOREIGN KEY ("CHAT_ID")
	  REFERENCES "DBI335167"."CHAT" ("CHAT_ID") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DBI335167"."PERSON_CHAT" ADD CONSTRAINT "FK_PERSON_CHAT_PERSON_EMAIL" FOREIGN KEY ("PERSON_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PERSON_HELP_SEEKER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_HELP_SEEKER" ADD CONSTRAINT "FK_PHS_PERSON_EMAIL" FOREIGN KEY ("PERSON_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PERSON_VOLUNTEER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."PERSON_VOLUNTEER" ADD FOREIGN KEY ("PERSON_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION" ADD CONSTRAINT "FK_QUESTION_HELPSEEKER_EMAIL" FOREIGN KEY ("HELPSEEKER_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DBI335167"."QUESTION" ADD CONSTRAINT "FK_QUESTION_VOLUNTEER_EMAIL" FOREIGN KEY ("VOLUNTEER_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION_OFFER
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION_OFFER" ADD FOREIGN KEY ("QUESTION_ID")
	  REFERENCES "DBI335167"."QUESTION" ("ID") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DBI335167"."QUESTION_OFFER" ADD FOREIGN KEY ("VOLUNTEER_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION_SKILL
--------------------------------------------------------

  ALTER TABLE "DBI335167"."QUESTION_SKILL" ADD CONSTRAINT "FK_QUESTION_SKILL_SKILL_ID" FOREIGN KEY ("SKILL_ID")
	  REFERENCES "DBI335167"."SKILL" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RESULTAAT
--------------------------------------------------------

  ALTER TABLE "DBI335167"."RESULTAAT" ADD FOREIGN KEY ("CODE")
	  REFERENCES "DBI335167"."MODULE" ("CODE") ENABLE;
 
  ALTER TABLE "DBI335167"."RESULTAAT" ADD FOREIGN KEY ("STUDNR")
	  REFERENCES "DBI335167"."STUDENT" ("STUDNR") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REVIEW
--------------------------------------------------------

  ALTER TABLE "DBI335167"."REVIEW" ADD CONSTRAINT "FK_REVIEW_VOLUNTEER_ID" FOREIGN KEY ("VOLUNTEER_EMAIL")
	  REFERENCES "DBI335167"."PERSON" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  DDL for Procedure EDIT_PLANNING
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "DBI335167"."EDIT_PLANNING" (
  v_email IN VARCHAR2,
  v_mo    IN VARCHAR2,
  v_tu    IN VARCHAR2,
  v_we    IN VARCHAR2,
  v_th    IN VARCHAR2,
  v_fr    IN VARCHAR2,
  v_sa    IN VARCHAR2,
  v_su    IN VARCHAR2 )  
IS  
  v_count NUMBER;
BEGIN

  SELECT COUNT(*) INTO v_count  
  FROM AVAILABILITY
  WHERE person_email = v_email;
  
  -- if user already in table
  -- remove all record first
  IF v_count > 0 THEN
    BEGIN
      DELETE FROM AVAILABILITY WHERE person_email = v_email;
    END;
  END IF; 
  
  -- add new records for each day of the week
  BEGIN
    INSERT INTO AVAILABILITY VALUES ( v_email, 'mo', v_mo );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'tu', v_tu );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'we', v_we );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'th', v_th );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'fr', v_fr );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'sa', v_sa );  
    INSERT INTO AVAILABILITY VALUES ( v_email, 'su', v_su );  
  END;  
END;

/
--------------------------------------------------------
--  DDL for Procedure IMPORT_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "DBI335167"."IMPORT_FILE" (
  v_upload_date IN DATE,
  v_content     IN FILES.CONTENT%type,
  v_type        IN VARCHAR2,
  v_name        IN VARCHAR2,
  v_length      IN NUMBER,
  v_email       IN PERSON.EMAIL%TYPE,
  v_id          OUT NUMBER)
IS
BEGIN
  SELECT files_seq.NEXTVAL INTO v_id FROM DUAL;
  
  INSERT INTO FILES 
  VALUES( v_id, v_upload_date, v_content, v_type, v_name, v_length );
  
  UPDATE PERSON
    SET FILE_ID = v_id
    WHERE EMAIL = v_email;
    
END IMPORT_FILE;

/
--------------------------------------------------------
--  DDL for Procedure IMPORT_VOG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "DBI335167"."IMPORT_VOG" (
  v_upload_date IN DATE,
  v_content     IN FILES.CONTENT%type,
  v_type        IN VARCHAR2,
  v_name        IN VARCHAR2,
  v_length      IN NUMBER,
  v_email       IN PERSON.EMAIL%TYPE,
  v_id          OUT NUMBER)
IS
BEGIN
  SELECT files_seq.NEXTVAL INTO v_id FROM DUAL;
  
  INSERT INTO FILES 
  VALUES( v_id, v_upload_date, v_content, v_type, v_name, v_length );
  
  UPDATE PERSON_VOLUNTEER
    SET VOG_ID = v_id
    WHERE PERSON_EMAIL = v_email;
    
END IMPORT_VOG;

/
