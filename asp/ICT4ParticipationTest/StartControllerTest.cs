﻿using System.Web.Mvc;
using Asp.Controllers;
using Asp.Data;
using Asp.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ICT4ParticipationTest
{
    /// <summary>
    /// This class is a test class to test StartController elements
    /// </summary>
    [TestClass]
    public class StartControllerTest
    {
        /// <summary>
        /// This field makes the repository functions in AccountRepository accessible
        /// </summary>
        private QuestionRepository questionRepo;

        /// <summary>
        /// This field makes the repository functions in ListRepository accessible
        /// </summary>
        private UserRepository userRepo;

        /// <summary>
        /// This method initializes elements that are used
        /// in the tests
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            questionRepo = new QuestionRepository(new QuestionOracleContext());
            userRepo = new UserRepository(new UserOracleContext());
        }

        /// <summary>
        /// This method tests if it is possible to sign in
        /// </summary>
        [TestMethod]
        public void SignIn()
        {
            var startController = new StartController();
            var result = startController.SignIn() as ViewResult;
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// This method tests if it is possible to sign in
        /// </summary>
        [TestMethod]
        public void SignUp()
        {
            var startController = new StartController();
            var result = startController.SignUp() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
