﻿$(document).ready(function() {
    var $adminContentWrapper = $('.admin-content-wrapper');
    var $adminHeaderLink = $('.admin-header-link');

    //// on questions header link click
    ////slidetoggle questions wrapper
    $adminHeaderLink.on('click', function () {
        
        ////get index clicked edit-icon
        var index = $adminHeaderLink.index(this);

        ////slidetoggle edit form
        $adminContentWrapper.eq(index).slideToggle('fast');
    });
});