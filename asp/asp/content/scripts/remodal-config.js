// remove remode url hash
// link: https://github.com/VodkaBears/Remodal 
window.REMODAL_GLOBALS = {
    NAMESPACE: 'modal',
    DEFAULTS: {
        hashTracking: false
    }
};
