$(document).ready(function () {
    var $edit = $('.edit-img');
    var $editForm = $('.edit-form');

   ////on edit-icon click
   ////slidetoggle edit-form
    $edit.on('click', function () {

        ////get index clicked edit-icon
        var index = $edit.index(this);

        ////slidetoggle edit form
        $editForm.eq(index).slideToggle('fast');
    });
});