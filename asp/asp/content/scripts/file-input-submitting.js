﻿$(document).ready(function() {
    var $profileImage = $("#profile-img");
    var $profileImageInput = $("#input-profile-img");
    var $profileImageForm = $("#form-profile-img");

    var fileSelected = null;

    // on profile image click
    // open file dialog
    $profileImage.on("click", function() {
        $profileImageInput.trigger("click");
    });

    // set fileselected
    $profileImageInput.on("click", function() {
        fileSelected = this.value;
        this.value = null;
    });

    // if filed is selected ->
    // submit profile image form
    $profileImageInput.on("change", function() {
        if (fileSelected != null) {
            $profileImageForm.submit();
        }
    });
});