$(document).ready(function () {
    var $menuIcon = $('#menu-icon');
    var $menuItems = $('.menu-item');

   ////toggle menu items
    $menuIcon.on('click', function () {
        $menuItems.slideToggle("fast");
    });

   ////show/hide menu items on window width 980
    $(window).resize(function () {
        if ($(this).width() >= 980) {
            $menuItems.css('display', 'inline');
        } else {
            $menuItems.css('display', 'none');
        }
    });
});