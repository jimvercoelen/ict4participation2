﻿/*
 * This script is for showing the validation messages
 * through a remodal
 */
$(document).ready(function () {
    var $validationModal = $('#validation-modal');
    var $validationModalTitle = $('#validation-modal-title');
    var $validationModalDescription = $('#validation-modal-description');

   ////remodal
    var validationRemodal = $validationModal.remodal();

    window.showOeps = function (message) {
        $validationModalTitle.html('Oeps!');
        $validationModalDescription.html(message);

        validationRemodal.open();
    }
});