$(document).ready(function () {
    var $action = $('.action');
    var $more = $('.more');

   ////on 'bekijk' click
   ////[1] toggle more (info) question
   ////[2] change 'bekijk' link text
    $action.on('click', function() {

       ////get index clicked 'bekijk' link
        var index = $action.index(this);

       ////[1]
        $more.eq(index).slideToggle('fast');

       ////'bekijk' link text
        var text = $action.eq(index).text();

       ////[2]
        $action.eq(index).text(text === 'Details' ? 'Verberg' : 'Details');
    });
});