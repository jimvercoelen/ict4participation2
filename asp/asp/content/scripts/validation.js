﻿$(document).ready(function () {
   ////logon
    var $inputEmail = $('#email');
    var $inputPassword = $('#password');
    var $btnSignIn = $('#btn-sign-in');
    var $btnSignUp = $('#btn-sign-up');

    var $lblHelpseeker = $('#label-helpseeker');
    var $lblVolunteer = $('#label-volunteer');

    var $askQuestionLink = $('#ask-question-link');
    var $askQuestionForm = $('#ask-question-form');

    var $inputTitle = $('#question-title');
    var $inputLocation = $('#question-location');
    var $inputContent = $('#question-content');

    var $reviewLink = $('#new-review-link');
    var $reviewRatingWrapper = $('#rating-wrapper');
    var $reviewForm = $('#review-form');
    var $reviewContent = $('#review-content');

    var $messageLink = $('#post-message-link');
    var $messageContent = $('#message-content');
    var $messageForm = $('#new-message-form');
    
    var prevent = false;
    
   ////shake element as notification
    function shakeInvalidInput($target) {
        $target.removeClass('shake');
        var reflow = $target[0].offsetHeight;
        $target.addClass('shake');
    }

   ////validate input field for value
   ////if empty -> shakeInvalidInput
    function validateInputField($input) {
        if ($input.val().length === 0) {
            prevent = true;
            shakeInvalidInput($input);
        }
    }

   ////btn sign in on click
   ////-> validateInputField
    $btnSignIn.on('click', function () {
        prevent = false;
        validateInputField($inputEmail);
        validateInputField($inputPassword);
        if (prevent) return false;
    });

   ////btn sign up on click
   ////-> validateInputField
    $btnSignUp.on('click', function () {
        prevent = false;
        validateInputField($inputEmail);
        validateInputField($inputPassword);

        ////separated validation on radiobuttons
        if (!$('[name="UserType"]:checked').val()) {
            shakeInvalidInput($lblHelpseeker);
            shakeInvalidInput($lblVolunteer);
            prevent = true;
        }
        if (prevent) return false;
    });

   ////ask question link on click
   ////submit ask question form
    $askQuestionLink.on('click', function () {
        prevent = false;
        validateInputField($inputTitle);
        validateInputField($inputLocation);
        validateInputField($inputContent);

        if (!prevent) {
            $askQuestionForm.submit();
        }
    });

    //// new review link on click
    //// review question form
    $reviewLink.on('click', function() {
        prevent = false;
        validateInputField($reviewContent);

        ////separated validation on radiobuttons
        if (!$('[name="rating"]:checked').val()) {
            shakeInvalidInput($reviewRatingWrapper);
            prevent = true;
        }

        if (!prevent) {
            $reviewForm.submit();
        }
    });

    //// new message link on click
    //// review question form
    $messageLink.on('click', function() {
        prevent = false;
        validateInputField($messageContent);

        if (!prevent) {
            $messageForm.submit();
        }
    });
});