﻿//-----------------------------------------------------------------------
// <copyright file="QuestionRepository.cs" company="Ict4Participation">
//     Participation
// </copyright>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using Asp.Data;
using Asp.Models;

namespace Asp.Logic
{
    /// <summary>
    /// This class contains all question methods for connecting with the database
    /// </summary>
    public class QuestionRepository
    {
        /// <summary>
        /// This property contains the context from the IQuestionContext interface
        /// </summary>
        private IQuestionContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionRepository"/> class
        /// </summary>
        /// <param name="context">IQuestionContext type context parameter</param>
        public QuestionRepository(IQuestionContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// This method will try and return the next sequence value for a new question
        /// </summary>
        /// <returns>Next question sequence/exception</returns>
        public int GetSequenceNextVal()
        {
            return this.context.GetSequenceNextVal();
        }

        /// <summary>
        /// This method try and insert a new question into the database 
        /// </summary>
        /// <param name="question">Question type question parameters</param>
        /// <returns>Boolean which if true if the inserting succeeded</returns>
        public bool AskQuestion(Question question)
        {
            return this.context.AskQuestion(question);
        }

        /// <summary>
        /// This method will try and render all questions if there a any in the database
        /// </summary>
        /// <returns>A list containing the questions/exception</returns>
        public List<Question> GetQuestions()
        {
            return this.context.GetQuestions();
        }

        /// <summary>
        /// This method will try and render all questions from a specific help seeker
        /// if there a any in the database
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A list containing the questions/exception</returns>
        public List<Question> GetQuestionsFromHelpseeker(string email)
        {
            return this.context.GetQuestionsFromHelpseeker(email);
        }

        /// <summary>
        /// This method will try and render a question depending on question id
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <returns>A single question instance/exception</returns>
        public Question RenderQuestion(int id)
        {
            return this.context.RenderQuestion(id);
        }

        /// <summary>
        /// This method will try and edit a question
        /// </summary>
        /// <param name="question">Question type question parameter</param>
        /// <returns>True when the question is edited</returns>
        public bool EditQuestion(Question question)
        {
            return this.context.EditQuestion(question);
        }

        /// <summary>
        /// This method will try and delete a question depending on the question id
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <returns>True when the question is deleted</returns>
        public bool DeleteQuestion(int id)
        {
            return this.context.DeleteQuestion(id);
        }

        /// <summary>
        /// This method will try and add an help offer to a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the help is offered</returns>
        public bool OfferHelp(int questionId, string volunteerEmail)
        {
            return this.context.OfferHelp(questionId, volunteerEmail);
        }

        /// <summary>
        /// This method will try and remove an help offer from a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the offered help is deleted</returns>
        public bool DeleteOfferedHelp(int questionId, string volunteerEmail)
        {
            return this.context.DeleteOfferedHelp(questionId, volunteerEmail);
        }

        /// <summary>
        /// This method will accept a volunteer to a question
        /// </summary>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <returns>True when the volunteer is accepted</returns>
        public bool AcceptVolunteer(int questionId, string volunteerEmail)
        {
            return this.context.AcceptVolunteer(questionId, volunteerEmail);
        }
    }
}