﻿//-----------------------------------------------------------------------
// <copyright file="ReviewRepository.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using asp.Data;
using asp.Models;
using Asp.Models.Users;
using System.Collections.Generic;

namespace asp.Logic
{
    public class ReviewRepository
    {
        /// <summary>
        /// This property contains the context from the IReviewContext interface
        /// </summary>
        private IReviewContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReviewRepository"/> class
        /// </summary>
        /// <param name="context">IReviewContext type context parameter</param>
        public ReviewRepository(IReviewContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// This method will try and return the next sequence value for a new review
        /// </summary>
        /// <returns>Next review sequence/exception</returns>
        public int GetSequenceNextVal()
        {
            return this.context.GetSequenceNextVal();
        }

        /// <summary>
        /// This method will post a review
        /// </summary>
        /// <param name="review"> review type review </param>
        /// <returns> true/ exception </returns>
        public bool PostReview(Review review)
        {
            return context.PostReview(review);
        }

        /// <summary>
        /// This method will get a list of reviews
        /// beloning to a single volunteer
        /// </summary>
        /// <param name="volunteerEmail"> string type volunteer's email </param>
        /// <returns> a list of reviews</returns>
        public List<Review> GetReviewsFromVolunteer(string volunteerEmail)
        {
            return context.GetReviewsFromVolunteer(volunteerEmail);
        }

        /// <summary>
        /// This method will search for the avg rating 
        /// from a specific volunteer
        /// </summary>
        /// <param name="volunteer"> volunteer type volunteer </param>
        /// <returns> int type avg rating </returns>
        public int AvgRatingVolunteer(Volunteer volunteer)
        {
            return context.AvgRatingVolunteer(volunteer);
        }
    }
}
