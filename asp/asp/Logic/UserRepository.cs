﻿//-----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Ict4Participation">
//     Participation
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Asp.Data;
using Asp.Models.Users;

namespace Asp.Logic
{
    /// <summary>
    /// This class contains all user methods for connecting with the database
    /// </summary>
    public class UserRepository
    {
        /// <summary>
        /// This property contains the context from the IUserContext interface
        /// </summary>
        private IUserContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class
        /// </summary>
        /// <param name="context">IUserContext type context parameter</param>
        public UserRepository(IUserContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// This method will try and find a user
        /// based on the input (email and password)
        /// if one is found, it will return this user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <returns>A user or an exception</returns>
        public User AuthorizedUser(string email, string password)
        {
            return this.context.AuthorizedUser(email, password);
        }

        /// <summary>
        /// This method will try and get a user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A user or an exception</returns>
        public User GetUser(string email)
        {
            return this.context.GetUser(email);
        }

        /// <summary>
        /// This method will get all helpseekers
        /// </summary>
        /// <returns> list helpseekers </returns>
        public List<HelpSeeker> GetAllHelpSeekers()
        {
            return this.context.GetAllHelpSeekers();
        }

        /// <summary>
        /// This method will get all volunteers
        /// </summary>
        /// <returns> list volunteers </returns>  
        public List<Volunteer> GetAllVolunteers()
        {
            return this.context.GetAllVolunteers();
        }

        /// <summary>
        /// This method will try and register a new user
        /// inside the database (after some checks)
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <returns>True when the user is registered</returns>
        public bool Register(User user)
        {
            return this.context.Register(user);
        }

        /// <summary>
        /// This method will try and edit a user's
        /// profile information
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <param name="oldName">string type old name parameter</param>
        /// <returns>True when the user's information is edited</returns>
        public bool EditProfileInfo(User user, string oldName)
        {
            return this.context.EditProfileInfo(user, oldName);
        }

        /// <summary>
        /// This method will try and edit a help seekers
        /// profile about information        
        /// </summary>
        /// <param name="helpSeeker">HelpSeeker type helpSeeker parameter</param>
        /// <returns>A HelpSeeker or an exception</returns>
        public bool EditProfileAboutHelpSeeker(HelpSeeker helpSeeker)
        {
            return this.context.EditProfileAboutHelpSeeker(helpSeeker);
        }

        /// <summary>
        /// This method will try and edit a volunteer
        /// profile about information        
        /// </summary>
        /// <param name="volunteer">Volunteer type volunteer parameter</param>
        /// <returns>A Volunteer or an exception</returns>
        public bool EditProfileAboutVolunteer(Volunteer volunteer)
        {
            return this.context.EditProfileAboutVolunteer(volunteer);
        }

        /// <summary>
        /// This method will try and edit the planning of a volunteer
        /// </summary>
        /// <param name="availabilities">A string array type 
        /// availabilities parameter containing seven availabilities, 
        /// one for each day of the week, starting at monday</param>
        /// <returns> true/ exception</returns>
        public bool EditPlanning(string[] availabilities)
        {
            return this.context.EditPlanning(availabilities);
        }

        /// <summary>
        /// This method wiil set a timeout on a user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="timeout"></param>
        /// <returns> true/ exception </returns>
        public bool SetTimeout(string email, DateTime timeout)
        {
            return this.context.SetTimeout(email, timeout);
        }

        /// <summary>
        /// This method will accept a volunteer in the system
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns> true/ exception </returns>
        public bool AcceptVolunteerVog(string email)
        {
            return this.context.AcceptVolunteerVog(email);
        }
    }
}