﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Asp.Data;
using Asp.Models;
using Asp.Models.Users;

namespace asp.Logic
{
    public class ChatRepository
    {
        private IChatContext context;

        public ChatRepository(IChatContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get a chat belonging to a user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <param name="name">string type name parameter</param>
        /// <returns>a chat</returns>
        public Chat GetChatFromPerson(string email, string name)
        {
            return context.GetChatFromPerson(email, name);
        }

        /// <summary>
        /// Gets all chats of the specified user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A list of chats</returns>
        public List<Chat> GetAllChatsFromPerson(string email)
        {
            return context.GetAllChatsFromPerson(email);
        }

        /// <summary>
        /// Get messages within a chat
        /// </summary>
        /// <param name="chatId"></param>
        /// <returns>list of messages</returns>
        public List<Message> GetMessagesFromChat(int chatId)
        {
            return context.GetMessagesFromChat(chatId);
        }

        public bool OpenNewChat(string volunteerEmail, string volunteerName, string helpSeekerEmail, string helpSeekerName)
        {
            return context.OpenNewChat(volunteerEmail, volunteerName, helpSeekerEmail, helpSeekerName);
        }

        public bool PostMessage(Message message, int chatId)
        {
            return context.PostMessage(message, chatId);
        }
    }
}