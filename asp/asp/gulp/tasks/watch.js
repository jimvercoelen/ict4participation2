var gulp = require('gulp');
var watch = require('gulp-watch');

var browserSync = require('./browser-sync');

gulp.task('watch', function () {
    watch([
        'content/styles/**/*.scss'
    ], function () {
        gulp.start('styles');
    });

    watch([
        'content/*.html',
        'content/images/**/*',
        'content/scripts/**/*'
    ], browserSync.reload)
});
