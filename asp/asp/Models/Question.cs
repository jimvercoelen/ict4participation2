﻿//-----------------------------------------------------------------------
// <copyright file="Question.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Asp.Models.Users;

namespace Asp.Models
{
    /// <summary>
    /// This class is a model for a Question
    /// </summary>
    public class Question
    {
        /// <summary>
        /// Initializes a new instance of the Question class
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <param name="helpSeekerEmail">string type helpSeekerEmail parameter</param>
        /// <param name="title">string type title parameter</param>
        /// <param name="content">string type content parameter</param>
        /// <param name="postDate">DateTime type postDate parameter</param>
        /// <param name="location">string type location parameter</param>
        /// <param name="status">string type status parameter</param>
        /// <param name="transport">string type transport parameter</param>
        public Question(
            int id, 
            string helpSeekerEmail, 
            string title, 
            string content, 
            DateTime postDate, 
            string location, 
            string status, 
            string transport)
        {
            this.Id = id;
            this.HelpSeekerEmail = helpSeekerEmail;
            this.Title = title;
            this.Content = content;
            this.PostDate = postDate;
            this.Location = location;
            this.Status = status;
            this.Transport = transport;
            this.HelpOfferedVolunteers = new List<Volunteer>();
        }

        /// <summary>
        /// Gets the id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the help seeker email
        /// </summary>
        public string HelpSeekerEmail { get; private set; }

        /// <summary>
        /// Gets the title
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets the content
        /// </summary>
        public string Content { get; private set; }

        /// <summary>
        /// Gets the post date
        /// </summary>
        public DateTime PostDate { get; private set; }

        /// <summary>
        /// Gets the location
        /// </summary>
        public string Location { get; private set; }

        /// <summary>
        /// Gets the status
        /// </summary>
        public string Status { get; private set; }

        /// <summary>
        /// Gets the transport
        /// </summary>
        public string Transport { get; private set; }

        /// <summary>
        /// Gets or sets the accepted volunteer
        /// </summary>
        public Volunteer AcceptedVolunteer { get; set; }

        /// <summary>
        /// Gets or sets the volunteers who offered help
        /// </summary>
        public List<Volunteer> HelpOfferedVolunteers { get; set; }

        /// <summary>
        /// This method will edit an questions
        /// </summary>
        /// <param name="title">string type title parameter</param>
        /// <param name="content">string type content parameter</param>
        /// <param name="location">string type location parameter</param>
        /// <param name="transport">string type transport parameter</param>
        public void EditQuestion(string title, string content, string location, string transport)
        {
            this.Title = title;
            this.Content = content;
            this.Location = location;
            this.Transport = transport;
        }
    }
}