﻿//-----------------------------------------------------------------------
// <copyright file="TransportOptions.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
namespace Asp.Models
{
    /// <summary>
    /// This enumeration specifies the way of transportation
    /// </summary>
    public enum TransportOptions
    {
        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is undefined
        /// </summary>
        Undefined,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by walking
        /// </summary>
        Walk,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by bike
        /// </summary>
        Bike,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by taxi
        /// </summary>
        Taxi,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by car
        /// </summary>
        Car,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by public transport
        /// </summary>
        PublicTransport,

        /// <summary>
        /// This enumeration element specifies that the way of transportation
        /// is by a special way of transportation
        /// </summary>
        Special
    }
}