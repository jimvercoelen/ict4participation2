﻿//-----------------------------------------------------------------------
// <copyright file="Review.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;

namespace asp.Models
{
    /// <summary>
    /// This class is a model for a Review
    /// </summary>
    public class Review
    {
        /// <summary>
        /// Initialize a new instance of the Review class
        /// </summary>
        /// <param name="helpSeekerName"> string type helpseeker's name</param>
        /// <param name="content">string type review content</param>
        /// <param name="rate"> int type rating </param>
        public Review(string helpSeekerName, string content, int rate)
        {
            this.HelpSeekerName = helpSeekerName;
            this.Content = content;
            this.Rating = rate;
        }

        /// <summary>
        /// Initialize a new instance of the Review class
        /// </summary>
        /// <param name="id"> int type id </param>
        /// <param name="helpSeekerName"> string type helpseeker's name</param>
        /// <param name="volunteerEmail">string type volunteer's name</param>
        /// <param name="content">string type review content</param>
        /// <param name="rating">double type rating </param>
        /// <param name="postdate">datetime type postdate</param>
        public Review(int id, string helpSeekerName, string volunteerEmail, string content, double rating, DateTime postdate)
        {
            this.Id = id;
            this.HelpSeekerName = helpSeekerName;
            this.VolunteerEmail = volunteerEmail;
            this.Content = content;
            this.Rating = rating;
            this.PostDate = postdate;
        }

        /// <summary>
        /// Gets or sets int type id property
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets string type helpseekername property
        /// </summary>
        public string HelpSeekerName { get; set; }

        /// <summary>
        /// Gets or sets volunteerEmail property
        /// </summary>
        public string VolunteerEmail { get; set; }

        /// <summary>
        /// Gets or sets content property
        /// </summary>
        public string Content { get; private set; }

        /// <summary>
        /// Gets or sets rating property
        /// </summary>
        public double Rating { get; private set; }

        /// <summary>
        /// Gets or sets postdate property
        /// </summary>
        public DateTime PostDate { get; set; }
    }
}
