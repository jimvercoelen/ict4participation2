﻿using System.Collections.Generic;

namespace Asp.Models
{
    public class Chat
    {
        public int Id { get; private set; } 

        public string Title { get; set; }

        public List<Message> Messages { get; set; }

        public Chat(int id, string title)
        {
            Messages = new List<Message>();
            Id = id;
            Title = title;
        }
    }
}