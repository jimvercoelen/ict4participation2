﻿//-----------------------------------------------------------------------
// <copyright file="TechnicalErrorMessage.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------

/* 
 * This class is for displaying (mainly database) exception messages
 * on a 'user-friendly' whay.
 */
using System;
using System.Text.RegularExpressions;

namespace Asp.Models
{
    /// <summary>
    /// This class is a model for a TechnicalErrorMessage
    /// </summary>
    public class TechnicalErrorMessage
    {
        /// <summary>
        /// Initializes a new instance of the TechnicalErrorMessage class
        /// </summary>
        /// <param name="e">Exception type e parameter</param>
        public TechnicalErrorMessage(Exception e)
        {
            this.ErrorMessage = "<br> " +
                           "Er is iets <b>mis</b> gegaan<br>" +
                           "Zorg ervoor dat u <i>verbonden</i> bent met het <b>internet</b> <br>" +
                           "en probeer <b>opnieuw</b><br><br>" +
                           "Voor de <i>techneuten</i> onderons<br>" +
                           "<span id='remodal-message'>" + Regex.Replace(e.Message, @"\t|\n|\r", string.Empty) + "</span>";
        }

        /// <summary>
        /// Gets the error message
        /// </summary>
        public string ErrorMessage { get; private set; }
    }
}