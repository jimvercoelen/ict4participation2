﻿//-----------------------------------------------------------------------
// <copyright file="Global.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Asp.Models.Users;

namespace Asp.Models
{
    /// <summary>
    /// This class contains all global needs 
    /// this way the application can access all needs from this class 
    /// </summary>
    public static class Global
    {
        /// <summary>
        /// Gets or sets authorizedUser
        /// </summary>
        public static User AuthorizedUser { get; set; }

        /// <summary>
        /// Gets or sets all questions
        /// </summary>
        public static List<Question> AllQuestions { get; set; }

        /// <summary>
        /// Gets or sets all volunteers
        /// </summary>
        public static List<Volunteer> AllVolunteers { get; set; }

        /// <summary>
        /// Gets or sets all helpseekers
        /// </summary>
        public static List<HelpSeeker> AllHelpseekers { get; set; }

        /// <summary>
        /// Gets or sets all chats
        /// </summary>
        public static List<Chat> AllChats { get; set; }

        /// <summary>
        /// Gets or sets all messages first chat
        /// </summary>
        public static List<Message> MessagesFirstChat { get; set; }

        /// <summary>
        /// This method acts as a 'TransportOption.ToString' method
        /// </summary>
        /// <param name="transport">TransportOption type transport parameter</param>
        /// <returns> transport to string </returns>
        public static string TransportToString(TransportOptions transport)
        {
            switch (transport)
            {
                case TransportOptions.Walk:
                    return "Te voet";
                case TransportOptions.Bike:
                    return "Fiets";
                case TransportOptions.Taxi:
                    return "Taxi";
                case TransportOptions.Car:
                    return "Auto";
                case TransportOptions.PublicTransport:
                    return "Openbaar vervoer";
                case TransportOptions.Special:
                    return "Speciaal vervoer";
                default:
                    return "n.v.t.";
            }
        }

        /// <summary>
        /// This method will get all error messages from a model state
        /// </summary>
        /// <param name="modelState"> model model state </param>
        /// <returns> list model state error messages </returns>
        internal static List<string> GetErrorListFromModelState(System.Web.Mvc.ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;

            return query.ToList();
        }

        /// <summary>
        /// Get the content of the file in byte array buffer
        /// </summary>
        /// <param name="path">File path on server</param>
        /// <returns>File content</returns>
        internal static byte[] GetFileContent(string path)
        {
            Stream fs = File.OpenRead(path);
            var buffer = new byte[fs.Length];
            fs.Read(buffer, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            return buffer;
        }
    }
}