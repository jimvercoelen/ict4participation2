﻿//-----------------------------------------------------------------------
// <copyright file="FileModel.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
namespace asp.Models
{
    /// <summary>
    /// This class contains a file modal properties
    /// </summary>
    public class FileModel
    {
        /// <summary>
        /// Gets or sets file modal contents property
        /// </summary>
        public byte[] FileContent { get; set; }

        /// <summary>
        /// Gets or sets file modal name property
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets file modal type property
        /// </summary>
        public string FileType { get; set; }
    }
}