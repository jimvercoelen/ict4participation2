﻿//-----------------------------------------------------------------------
// <copyright file="Volunteer.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using asp.Models;

namespace Asp.Models.Users
{
    /// <summary>
    /// This class is a model for a Volunteer
    /// </summary>
    public class Volunteer : User
    {
        /// <summary>
        /// Initializes a new instance of the Volunteer class
        /// </summary>
        /// <param name="userType">UserType type userType parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <param name="name">string type name</param>
        /// <param name="bio">string type bio</param>
        /// <param name="location">string type location</param>
        /// <param name="dateOfBirth">nullable datetime type date of birth</param>
        /// <param name="timeoutTill">nullable datetime type time out till</param>
        /// <param name="hasDrivingLicense">nullable bool type has car driving license</param>
        /// <param name="hasCar">nullable bool type has car</param>
        /// <param name="vogAccepted">nullable bool type vog accepted</param>
        public Volunteer(
            UserType userType, 
            string email, 
            string password,
            string name,
            string bio,
            string location,
            DateTime? dateOfBirth,
            DateTime? timeoutTill,
            bool? hasDrivingLicense, 
            bool? hasCar, 
            bool? vogAccepted)
            : base(userType, email, password, name, bio, location, dateOfBirth, timeoutTill)
        {
            this.Availabilities = new string[7] {"-", "-", "-", "-", "-", "-", "-" };
            this.HasDrivingLicense = hasDrivingLicense;
            this.HasCar = hasCar;
            this.Vog = this.ProfileImage = new FileModel();
            this.VogAccepted = vogAccepted;
        }

        /// <summary>
        /// Gets or sets whether the volunteer has a driving license or not
        /// </summary>
        public bool? HasDrivingLicense { get; set; }

        /// <summary>
        /// Gets or sets whether the volunteer has a car or not
        /// </summary>
        public bool? HasCar { get; set; }

        /// <summary>
        /// Gets or sets whether the VOG of the volunteer is accepted
        /// or not
        /// </summary>
        public bool? VogAccepted { get; set; }

        /// <summary>
        /// Gets or sets the availabilities of a volunteer
        /// </summary>
        public string[] Availabilities { get; set; }

        /// <summary>
        /// Gets or sets the Reviews from a volunteer
        /// </summary>
        public List<Review> Reviews { get; set; }

        /// <summary>
        /// Gets or sets Vog property
        /// </summary>
        public FileModel Vog { get; set; }

        /// <summary>
        /// This method will try and edit the about part of the 
        /// profile of a volunteer
        /// </summary>
        /// <param name="city">string type city parameters</param>
        /// <param name="dateOfBirth">DateTime? type dateOfBirth parameters</param>
        /// <param name="publicTransport">boolean? type publicTransport parameters</param>
        /// <param name="hasDrivingLicense">boolean? type hasDrivingLicense parameters</param>
        /// <param name="hasCar">boolean? type hasCar parameters</param>
        public override void EditProfileAbout(string city, DateTime? dateOfBirth, bool? publicTransport, bool? hasDrivingLicense, bool? hasCar)
        {
            base.EditProfileAbout(city, dateOfBirth, publicTransport, hasDrivingLicense, hasCar);
            this.HasDrivingLicense = hasDrivingLicense;
            this.HasCar = hasCar;
        }
    }
}