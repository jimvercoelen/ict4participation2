﻿//-----------------------------------------------------------------------
// <copyright file="UserType.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
namespace Asp.Models.Users
{
    /// <summary>
    /// This enumeration specifies the type of the user account
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// This enumeration element specifies that the type of
        /// the user account is admin
        /// </summary>
        Admin,

        /// <summary>
        /// This enumeration element specifies that the type of
        /// the user account is help seeker
        /// </summary>
        Helpseeker,

        /// <summary>
        /// This enumeration element specifies that the type of
        /// the user account is volunteer
        /// </summary>
        Volunteer,

        /// <summary>
        /// This enumeration element specifies that the type of
        /// the user account is undefined
        /// </summary>
        Undefined
    }
}