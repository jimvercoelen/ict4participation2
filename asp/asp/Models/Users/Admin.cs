﻿//-----------------------------------------------------------------------
// <copyright file="Admin.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
namespace Asp.Models.Users
{
    /// <summary>
    /// This class is a model for an Admin
    /// </summary>
    public class Admin : User
    {
        /// <summary>
        /// Initializes a new instance of the Admin class
        /// </summary>
        /// <param name="userType">UserType type userType parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        public Admin(UserType userType, string email, string password)
            : base(userType, email, password)
        {
        }
    }
}