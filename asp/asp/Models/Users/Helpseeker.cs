﻿//-----------------------------------------------------------------------
// <copyright file="HelpSeeker.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;

namespace Asp.Models.Users
{
    /// <summary>
    /// This class is a model for a HelpSeeker
    /// </summary>
    public class HelpSeeker : User
    {
        /// <summary>
        /// Initializes a new instance of the HelpSeeker class
        /// </summary>
        /// <param name="userType">UserType type userType parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <param name="name">string type name</param>
        /// <param name="bio">string type bio</param>
        /// <param name="location">string type location</param>
        /// <param name="dateOfBirth">nullable datetime type date of birth</param>
        /// <param name="timeoutTill">nullable datetime type time out till</param>
        /// <param name="canUsePublicTransport">nullable bool type can use public transport</param>
        public HelpSeeker(
            UserType userType, 
            string email, 
            string password,
            string name,
            string bio,
            string location,
            DateTime? dateOfBirth,
            DateTime? timeoutTill,
            bool? canUsePublicTransport)
            : base(userType, email, password, name, bio, location, dateOfBirth, timeoutTill)
        {
            CanUsePublicTransport = canUsePublicTransport;
        }

        /// <summary>
        /// Gets or sets whether the help seeker can use
        /// public transport
        /// </summary>
        public bool? CanUsePublicTransport { get; set; }

        /// <summary>
        /// This method will try and edit the about part of the 
        /// profile of a volunteer
        /// </summary>
        /// <param name="city">string type city parameters</param>
        /// <param name="dateOfBirth">DateTime? type dateOfBirth parameters</param>
        /// <param name="publicTransport">boolean? type publicTransport parameters</param>
        /// <param name="hasDrivingLicense">boolean? type hasDrivingLicense parameters</param>
        /// <param name="hasCar">boolean? type hasCar parameters</param>
        public override void EditProfileAbout(
            string city, DateTime? dateOfBirth, bool? publicTransport, bool? hasDrivingLicense, bool? hasCar)
        {
            base.EditProfileAbout(city, dateOfBirth, publicTransport, hasDrivingLicense, hasCar);
            this.CanUsePublicTransport = publicTransport;
        }
    }
}