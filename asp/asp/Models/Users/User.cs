﻿//-----------------------------------------------------------------------
// <copyright file="User.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.ComponentModel.DataAnnotations;
using asp.Models;

namespace Asp.Models.Users
{
    /// <summary>
    /// This class is a model for a User
    /// </summary>
    public class User
    {
        /// <summary>
        /// Initializes a new instance of the User class <see cref="User"/>
        /// </summary>
        public User()
        {
            this.ProfileImage = new FileModel();
        }

        /// <summary>
        /// Initializes a new instance of the User class <see cref="User"/>
        /// </summary>
        /// <param name="userType">UserType type userType parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        public User(UserType userType,
            string email,
            string password)
        {
            this.UserType = userType;
            this.Email = email;
            this.Password = password;
            this.ProfileImage = new FileModel();
        }

        /// <summary>
        /// Initializes a new instance of the User class <see cref="User"/>
        /// </summary>
        /// <param name="userType">UserType type userType parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <param name="name">string type name</param>
        /// <param name="bio">string type bio</param>
        /// <param name="location">string type location</param>
        /// <param name="dateOfBirth">nullable datetime type date of birth</param>
        /// <param name="timeoutTill">nullable datetime type time out till</param>
        public User(
            UserType userType, 
            string email, 
            string password,
            string name,
            string bio,
            string location,
            DateTime? dateOfBirth,
            DateTime? timeoutTill)
        {
            this.UserType = userType;
            this.Email = email;
            this.Password = password;
            this.Name = name;
            this.Bio = bio;
            this.Location = location;
            this.DateOfBirth = dateOfBirth;
            this.TimeoutTill = timeoutTill;
            this.ProfileImage = new FileModel();
        }

        /// <summary>
        /// Gets or sets the user type
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
            ErrorMessage = "<b>Ongeldig</b> <i>emailadres</i><br>")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        [DataType(DataType.Text)]
        [RegularExpression(@"^(?=.*\d).{4,20}$", 
            ErrorMessage = "Het gekozen <b>wachtwoord</b> is niet <i>strerk</i> genoeg<br>")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the bio
        /// </summary>
        public string Bio { get; set; }

        /// <summary>
        /// Get or sets the location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the date of birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the date until the time out is released
        /// </summary>
        public DateTime? TimeoutTill { get; set; }

        /// <summary>
        /// Gets or sets HasTimeout property
        /// </summary>
        public bool HasTimeout { get; set; }

        /// <summary>
        /// Gets or sets whether the user wan't to remember logon
        /// </summary>
        public bool Remember { get; set; }

        /// <summary>
        /// Gets or sets profile image property
        /// </summary>
        public FileModel ProfileImage { get; set; }

        /// <summary>
        /// This method will try and edit the about part of the 
        /// profile of a volunteer
        /// </summary>
        /// <param name="city">string type city parameters</param>
        /// <param name="dateOfBirth">DateTime? type dateOfBirth parameters</param>
        /// <param name="publicTransport">boolean? type publicTransport parameters</param>
        /// <param name="hasDrivingLicense">boolean? type hasDrivingLicense parameters</param>
        /// <param name="hasCar">boolean? type hasCar parameters</param>
        public virtual void EditProfileAbout(
            string city, 
            DateTime? dateOfBirth, 
            bool? publicTransport, 
            bool? hasDrivingLicense, 
            bool? hasCar)
        {
            //// Check for empty input
            //// If empty, it means it doesn't need to be edited
            this.Location = string.IsNullOrEmpty(city) ? Location : city;
            this.DateOfBirth = dateOfBirth ?? this.DateOfBirth; 
        }

        /// <summary>
        /// This method will try and edit the profile of a volunteer
        /// </summary>
        /// <param name="name">string type name parameters</param>
        /// <param name="bio">string type bio parameters</param>
        public void EditProfileInfo(string name, string bio)
        {
            ////Check for empty input
            ////(if those are empty, it meens they don't need to be edited)
            this.Name = string.IsNullOrEmpty(name) ? this.Name : name;
            this.Bio = string.IsNullOrEmpty(bio) ? this.Bio : bio;
        }
    }
}