﻿using System;

namespace Asp.Models
{
    public class Message
    {
        public int Id { get; private set; }
        public string Content { get; private set; }
        public string Name { get; private set; }
        public DateTime DateTime { get; private set; }

        public Message(int id, string name, string content, DateTime dateTime)
        {
            Id = id;
            Content = content;
            Name = name;
            DateTime = dateTime;
        }

        public Message(string name, string content, DateTime dateTime)
        {
            Content = content;
            Name = name;
            DateTime = dateTime;
        }
    }
}