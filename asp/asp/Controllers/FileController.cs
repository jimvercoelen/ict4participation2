﻿using System;
using System.Configuration;
//-----------------------------------------------------------------------
// <copyright file="FileController.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System.IO;
using System.Web;
using System.Web.Mvc;
using asp.Data;
using asp.Models;
using Asp.Models;
using Asp.Models.Users;

namespace asp.Controllers
{
    /// <summary>
    /// This class is the file controller
    /// the file controller contains all methods which are needed to upload a file into the database
    /// (e.g. profile image)
    /// </summary>
    public class FileController : Controller
    {
        /// <summary>
        /// this action result will save a profile image into the database
        /// </summary>
        /// <returns> redirect dashboard, home controller </returns>
        [HttpPost]
        public ActionResult UploadProfileImage()
        {
            try
            {
                this.Import(this.Request, "IMPORT_FILE");
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                ViewBag.ShowRemodal = "Show";
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard", "Home");
        }

        /// <summary>
        /// this action result will save a VOG into the database
        /// </summary>
        /// <returns> redirect dashboard, home controller </returns>
        [HttpPost]
        public ActionResult UploadVog()
        {
            try
            {
                this.Import(this.Request, "IMPORT_VOG");
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                ViewBag.ShowRemodal = "Show";
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard", "Home");
        }

        /// <summary>
        /// this method will save a profile image into the database
        /// </summary>
        /// <param name="request"> Http base request</param>
        /// <param name="type">string type type parameter</param>
        public void Import(HttpRequestBase request, string type)
        {
            foreach (string fileName in request.Files)
            {
                //// get the file the user selected
                var file = request.Files[fileName];

                if (file.ContentLength <= 0)
                {
                    continue;
                }

                //// get the name of the folder in which the file will be saved on the server
                var saveFolder = ConfigurationManager.AppSettings["savefolder"];

                // creating the path for the file on the server
                var filePath = Path.Combine(
                    request.ServerVariables["APPL_PHYSICAL_PATH"] + saveFolder, 
                    Path.GetFileName(file.FileName));

                //// saving a copy of the user's posted file on the server
                file.SaveAs(filePath);

                // save the file in the database
                var id = ImportExportFileOracleContext.ImportFile(
                    DateTime.Now,
                    file.FileName,
                    filePath,
                    file.ContentType,
                    file.ContentLength,
                    Global.AuthorizedUser.Email,
                    null,
                    type);

                //// reload profile image current user (globale authorized user) 
                //// without reloading for the databse
                if (type == "IMPORT_FILE")
                {
                    Global.AuthorizedUser.ProfileImage = new FileModel()
                    {
                        FileContent = Global.GetFileContent(filePath),
                        FileName = file.FileName,
                        FileType = file.ContentType
                    };
                }
                
                //// deleting the file just imported so that the sever disk does not get full
                System.IO.File.Delete(filePath);
            }
        }

        public ActionResult GetPdf(string filePath)
        {
            var fileStream = new FileStream(filePath,
                                             FileMode.Open,
                                             FileAccess.Read
                                           );
            var fsResult = new FileStreamResult(fileStream, "application/pdf");
            return fsResult;
        }
    }
}