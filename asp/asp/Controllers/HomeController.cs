﻿//-----------------------------------------------------------------------
// <copyright file="HomeController.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Linq;
using System.Web.Mvc;
using asp.Data;
using Asp.Data;
using asp.Logic;
using Asp.Logic;
using Asp.Models;
using Asp.Models.Users;
using asp.Models;

namespace Asp.Controllers
{
    /// <summary>
    /// This class is the home controller
    /// the home controller contains all methods which are needed in the dashboard view
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// This repository contains all (user) methods for communicating with the database
        /// </summary>
        private readonly UserRepository userRepository = new UserRepository(new UserOracleContext());

        /// <summary>
        /// This repository contains all (question) methods for communicating with the database
        /// </summary>
        private readonly QuestionRepository questionRepository = new QuestionRepository(new QuestionOracleContext());
        
        /// <summary>
        /// This repository contains all (review) methods for communicating with the database
        /// </summary>
        private readonly ReviewRepository reviewRepository = new ReviewRepository(new ReviewOracleContext());

        /// <summary>
        /// This repository contains all (chat) methods for communicating with the database
        /// </summary>
        private readonly ChatRepository chatRepository = new ChatRepository(new ChatOracleContext());

        /// <summary>
        /// This action result will load the users dashboard
        /// unless global authorized user isn't set (user is not loged in)
        /// </summary>
        /// <returns> return view Dashboard/ redirect action sign in</returns>
        public ActionResult Dashboard()
        {
            if (Global.AuthorizedUser == null)
            {
                return this.RedirectToAction("SignIn", "Start");
            }

            try
            {
                //// depending on the usertype get
                //// the correct context will be loaded 
                switch (Global.AuthorizedUser.UserType)
                {
                    case UserType.Helpseeker:
                        Global.AllQuestions = this.questionRepository.GetQuestionsFromHelpseeker(Global.AuthorizedUser.Email);
                        if (this.TempData["ShowChat"] == null)
                        {
                            Global.AllChats = this.chatRepository.GetAllChatsFromPerson(Global.AuthorizedUser.Email);
                        }
                        break;

                    case UserType.Volunteer:
                        Global.AllQuestions = this.questionRepository.GetQuestions();
                        ((Volunteer) Global.AuthorizedUser).Reviews =
                            this.reviewRepository.GetReviewsFromVolunteer(Global.AuthorizedUser.Email);
                        if (this.TempData["ShowChat"] == null)
                        {
                            Global.AllChats = this.chatRepository.GetAllChatsFromPerson(Global.AuthorizedUser.Email);
                        }
                        break;

                    case UserType.Admin:
                        Global.AllQuestions = this.questionRepository.GetQuestions();
                        Global.AllHelpseekers = this.userRepository.GetAllHelpSeekers();
                        Global.AllVolunteers = this.userRepository.GetAllVolunteers();
                        
                        //// load all reviews for each volunteer
                        foreach (var volunteer in Global.AllVolunteers)
                        {
                            volunteer.Reviews = this.reviewRepository.GetReviewsFromVolunteer(volunteer.Email);
                        }

                        break;
                }
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
                return this.RedirectToAction("SignIn", "Start");
            }


            //// depending on usertype show correct dashboard
            if (Global.AuthorizedUser.UserType == UserType.Helpseeker)
            {
                return this.View("HelpseekerDashboard", Global.AuthorizedUser);
            }
            else if (Global.AuthorizedUser.UserType == UserType.Volunteer)
            {
                return this.View("VolunteerDashboard", Global.AuthorizedUser);
            }
            else
            {
                return this.View("AdminDashboard");
            }
        }

        /// <summary>
        /// This action result will try and edit an user's profile info
        /// </summary>
        /// <param name="name"> input (new) name</param>
        /// <param name="bio">input (new) biography</param>
        /// <returns> redirect action dashboard </returns>
        [HttpPost]
        public ActionResult EditProfileInfo(string name, string bio)
        {
            try
            {
                //// Edit user on current session (global auhorized user) and in database
                var oldName = Global.AuthorizedUser.Name;
                Global.AuthorizedUser.EditProfileInfo(name, bio);
                this.userRepository.EditProfileInfo(Global.AuthorizedUser, oldName);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and edit an user's (about) profile info
        /// </summary>
        /// <param name="city"> input (new) city</param>
        /// <param name="dateOfBirth"> input (new) date of birth</param>
        /// <param name="publicTransport"> input (new) can use public transport state</param>
        /// <param name="hasDrivingLicense"> input (new) has driving license state</param>
        /// <param name="hasCar"> input (new) has car state</param>
        /// <returns> redirect action dashboard </returns>
        [HttpPost]
        public ActionResult EditProfileAbout(
            string city, string dateOfBirth, bool? publicTransport, bool? hasDrivingLicense, bool? hasCar)
        {
            try
            {              
                //// Edit user in the database and the (global authorized) user
                //// this way the user won't need to reload from the database
                //// (depending on what user type)
                switch (Global.AuthorizedUser.UserType)
                {   
                    case UserType.Helpseeker:
                        ((HelpSeeker)Global.AuthorizedUser).EditProfileAbout(city, Convert.ToDateTime(dateOfBirth), publicTransport, null, null);
                        this.userRepository.EditProfileAboutHelpSeeker((HelpSeeker) Global.AuthorizedUser);
                        break;

                    case UserType.Volunteer:
                        ((Volunteer)Global.AuthorizedUser).EditProfileAbout(city, Convert.ToDateTime(dateOfBirth), null, hasDrivingLicense, hasCar);
                        this.userRepository.EditProfileAboutVolunteer((Volunteer) Global.AuthorizedUser);
                        break;
                }
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and edit the planning of a volunteer
        /// </summary>
        /// <param name="monday"> monday availability</param>
        /// <param name="tuesday"> tuesday availability</param>
        /// <param name="wednesday"> wednesday availability</param>
        /// <param name="thursday"> thursday availability</param>
        /// <param name="friday"> friday availability</param>
        /// <param name="saturday"> saturday availability</param>
        /// <param name="sunday"> sunday availability</param>
        /// <returns> redirect action dashboard </returns>
        [HttpPost]
        public ActionResult EditPlanning(
            string monday,
            string tuesday,
            string wednesday,
            string thursday,
            string friday,
            string saturday,
            string sunday)
        {
            //// put input into list
            var availabilities = new string[]
            {
                monday,
                tuesday,
                wednesday,
                thursday,
                friday,
                saturday,
                sunday
            };

            try
            {
                //// Edit (global authorized) volunteers planning
                //// this way the user won't need to reload from the database
                ((Volunteer) Global.AuthorizedUser).Availabilities = availabilities;

                //// Edit volunteer into the database
                this.userRepository.EditPlanning(availabilities);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and add a new question into the database
        /// after some validation checks 
        /// </summary>
        /// <param name="title"> question title </param>
        /// <param name="content"> question content </param>
        /// <param name="location"> question location</param>
        /// <param name="transport"> question transport type</param>
        /// <returns> redirect action dashboard </returns>
        [HttpPost]
        public ActionResult PostQuestion(string title, string content, string location, TransportOptions transport)
        {
            var isValid = true;
            var message = string.Empty;

            //// validation
            if (title.Length > 45)
            {
                isValid = false;
                message += "<i>Titel</i> is te lang<br>";
            }

            if (content.Length < 25)
            {
                isValid = false;
                message += "<i>Beschrijving</i> is te kort<br>";
            }

            if (!isValid)
            {
                //// show invalid message through remodal
                this.TempData["RemodalMessage"] = message + "<br><b>Probeer opnieuw</b>";
                return this.RedirectToAction("Dashboard");
            }

            try
            {
                var question = new Question(
                    this.questionRepository.GetSequenceNextVal(), 
                    Global.AuthorizedUser.Email,
                    title,
                    content,
                    DateTime.Now, 
                    location,
                    "Open",
                    Global.TransportToString(transport));

                this.questionRepository.AskQuestion(question);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and edit a question
        /// </summary>
        /// <param name="id"> question id </param>
        /// <param name="title"> input (edited) question title </param>
        /// <param name="content"> input (edited) question content </param>
        /// <param name="location"> input (edited) question location </param>
        /// <param name="transportOption"> input (edited) question transport option</param>
        /// <returns> redirect action dashboard </returns>
        [HttpPost]
        public ActionResult EditQuestion(int id, string title, string content, string location, TransportOptions transportOption)
        {
            try
            {
                foreach (var question in Global.AllQuestions.Where(q => q.Id == id))
                {
                    question.EditQuestion(title, content, location, Global.TransportToString(transportOption));
                    this.questionRepository.EditQuestion(question);
                }
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and delete a question
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <param name="status">string type question status</param>
        /// <param name="volunteerEmail">string type email volunteer</param>
        /// <returns> redirect action dashboard </returns>
        public ActionResult DeleteQuestion(int id, string status, string volunteerEmail)
        {
            try
            {
                this.questionRepository.DeleteQuestion(id);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and add an help offer to a question 
        /// </summary>
        /// <param name="id"> question id </param>
        /// <returns> redirect action dashboard </returns>
        public ActionResult OfferHelp(int id)
        {
            try
            {
                this.questionRepository.OfferHelp(id, Global.AuthorizedUser.Email);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and remove an help offer from a question 
        /// </summary>
        /// <param name="id"> question id </param>
        /// <returns> redirect action dashboard </returns>
        public ActionResult DeleteOfferedHelp(int id)
        {
            try
            {
                this.questionRepository.DeleteOfferedHelp(id, Global.AuthorizedUser.Email);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will get a user depending on the email (input)
        /// sets the 'ViewBag showuser' and redirect back to dashboard
        /// when this temp data is set, a 'remodal' and will show a user's info
        /// </summary>
        /// <param name="email"> user email </param>
        /// <param name="questionId"> question id </param>
        /// <returns>a redirect action dashboard</returns>
        public ActionResult ShowUser(string email, int questionId)
        {
            try
            {
                var user = userRepository.GetUser(email);

                //// if user to show is volunteer, load all reviews first
                if (user is Volunteer)
                {
                    ((Volunteer) user).Reviews = this.reviewRepository.GetReviewsFromVolunteer(user.Email);
                    this.TempData["UserToShow"] = user;
                }
                else
                {
                    this.TempData["UserToShow"] = user;
                }

                this.TempData["QuestionId"] = questionId;
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will accept a volunteer to a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="email">string type email parameter</param>
        /// <param name="volName">string type volName parameter</param>
        /// <returns>Redirect action to dashboard</returns>
        public ActionResult AcceptVolunteer(int questionId, string email, string volName)
        {
            try
            {
                this.questionRepository.AcceptVolunteer(questionId, email);
                var chat = chatRepository.GetChatFromPerson(email, Global.AuthorizedUser.Name);
                if (chat == null)
                {
                    this.chatRepository.OpenNewChat(email, volName, Global.AuthorizedUser.Email, Global.AuthorizedUser.Name);
                }

                //Global.AllChats = this.chatRepository.GetAllChatsFromPerson(Global.AuthorizedUser.Email);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will set a timeout
        /// </summary>
        /// <param name="email">string type user email </param>
        /// <param name="timeout">string type timeout time</param>
        /// <returns>Redirect action to dashboard</returns>
        [HttpPost]
        public ActionResult SetUserTimeout(string email, string timeout)
        {
            if (string.IsNullOrEmpty(timeout))
            {
                return this.View("AdminDashboard");
            }

            try
            {
                var timeoutDuration = Convert.ToDateTime(timeout);
                this.userRepository.SetTimeout(email, timeoutDuration);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will set TempDate["VolunteerEmail"]
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowReview(int questionId, string volunteerEmail)
        {
            TempData["QuestionId"] = questionId;
            TempData["VolunteerEmail"] = volunteerEmail;

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and insert a new review into the database
        /// </summary>
        /// <returns>Redirect action to dashboard</returns>
        [HttpPost]
        public ActionResult PostReview(int questionId, string rating, string content, string volunteerEmail)
        {
            try
            {
                this.questionRepository.DeleteQuestion(questionId);


                var review = new Review(
                    this.reviewRepository.GetSequenceNextVal(),
                    Global.AuthorizedUser.Name,
                    volunteerEmail, 
                    content, 
                    Convert.ToDouble(rating), 
                    DateTime.Now);

                this.reviewRepository.PostReview(review);
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will get all messages selected chat
        /// and will also set this chat als first
        /// and set TemData["ShowChat"]
        /// </summary>
        /// <param name="chatId">int type chat id parameter</param>
        /// <returns></returns>
        public ActionResult ChatToShow(string chatId)
        {
            try
            {
                var id = Convert.ToInt32(chatId);

                //// get all messages from selected chat
                Global.MessagesFirstChat = this.chatRepository.GetMessagesFromChat(id);
                
                //// find index and chat within Global Allchats
                var index = Global.AllChats.FindIndex(c => c.Id == id);
                var item = Global.AllChats[index];

                //// remove item and insert at top
                Global.AllChats.RemoveAt(index);
                Global.AllChats.Insert(0, item);

                this.TempData["ShowChat"] = true;
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will try and post a new message into the database
        /// </summary>
        /// <param name="chatId"> string type chat id parameter </param>
        /// <param name="content"> string type content parameter</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PostMessage(string chatId, string content)
        {
            try
            {
                var message = new Message(Global.AuthorizedUser.Name, content, DateTime.Now);
                this.chatRepository.PostMessage(message, Convert.ToInt32(chatId));

                //// reload messages from chat
                Global.MessagesFirstChat = this.chatRepository.GetMessagesFromChat(Convert.ToInt32(chatId));
                this.TempData["ShowChat"] = true;
            }
            catch (Exception e)
            {
                //// show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
            }

            return this.RedirectToAction("Dashboard");
        }

        /// <summary>
        /// This action result will enable the admin to accept a volunteer and grant him
        /// extra functions
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>Redirect action to dashboard</returns>
        public ActionResult AcceptVolunteerVog(string email)
        {
            userRepository.AcceptVolunteerVog(email);
            return this.RedirectToAction("Dashboard");
        }
    }
}