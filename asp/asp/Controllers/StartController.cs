﻿//-----------------------------------------------------------------------
// <copyright file="StartController.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Asp.Data;
using Asp.Logic;
using Asp.Models;
using Asp.Models.Users;

namespace Asp.Controllers
{
    /// <summary>
    /// This class is the start (up) controller
    /// the home controller contains all methods which are needed in the sign in and sign up views
    /// </summary>
    public class StartController : Controller
    {
        /// <summary>
        /// This repository contains all (user) methods for communicating with the database
        /// </summary>
        private readonly UserRepository userRepository = new UserRepository(new UserOracleContext());

        /// <summary>
        /// This action result will get the sign in view
        /// if there is a saved cookie containing a user email -> 
        /// get user and redirect dashboard 
        /// </summary>
        /// <returns> sign in view </returns>
        public ActionResult SignIn()
        {
            var httpCookie = this.ControllerContext.HttpContext.Request.Cookies["user"];

            if (httpCookie != null)
            {
                try
                {
                    var user = userRepository.GetUser(httpCookie.Value);

                    if (!TimeoutCheck(user))
                    {
                        Global.AuthorizedUser = user;
                        return this.RedirectToAction("Dashboard", "Home");
                    }
                }
                catch (Exception e)
                {
                    //// Show exception message through remodal
                    this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
                    return this.View();
                }
            }

            return this.View();
        }

        /// <summary>
        /// This action result will try and authorize an user
        /// by comparing the email en password from the database
        /// </summary>
        /// <param name="user"> user to logon </param>
        /// <returns> sign in view </returns>
        [HttpPost]
        public ActionResult SignIn(User user)
        {
            try
            {
                var userr = this.userRepository.AuthorizedUser(user.Email, user.Password);

                //// check if user exists
                if (userr != null)
                {
                    //// check if user has no timeout
                    if (!TimeoutCheck(userr))
                    {
                        //// set authorized user
                        Global.AuthorizedUser = userr;

                        //// set user cookie if user wants to remember
                        if (user.Remember)
                        {
                            var cookie = new HttpCookie("user") {Value = user.Email};
                            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                        }

                        return this.RedirectToAction("Dashboard", "Home");
                    }
                    else
                    {
                        //// return view with message timeout
                        return this.View();
                    }
                }

                //// message invalid email/password
                this.TempData["RemodalMessage"] = 
                    "De <b>combinatie</b> van het <i>email</i> en <i>wachtwoord</i>  <br> " +
                    "wordt <b>niet</b> niet herkend <br>" +
                    "Probeer het opnieuw";
                return this.View();
            }
            catch (Exception e)
            {
                //// Show exception message through remodal
                this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
                return this.View();
            }
        }

        /// <summary>
        /// This action result will get the sign up view
        /// </summary>
        /// <returns> sign up view </returns>
        public ActionResult SignUp()
        {
            return this.View();
        }

        /// <summary>
        /// This action result will try and register a new user
        /// after doing some validation checks
        /// </summary>
        /// <param name="user"> user to register </param>
        /// <returns> sign in/ sign up view </returns>
        [HttpPost]
        public ActionResult SignUp(User user)
        {
            var message = "";

            if (ModelState.IsValid)
            {
                try
                {
                    this.userRepository.Register(user);
                    return this.View("SignIn");
                }
                catch (Exception e)
                {
                    //// show exception message through remodal
                    //// if exception message is about email constraint show custom message 
                    if (e.Message.ToLower().Contains("unique constraint"))
                    {
                        this.TempData["RemodalMessage"] = "Het emailadres is al geregistreerd";
                    }
                    else
                    {
                        this.TempData["RemodalMessage"] = new TechnicalErrorMessage(e).ErrorMessage;
                    }
                }
            }
            else
            {
                //// Get all invalid error messages and set them into TempDate["RemodalMessage"]
                var errorMessage = Global.GetErrorListFromModelState(ModelState);
                message = errorMessage.Aggregate(message, (current, error) => current + error);
                this.TempData["RemodalMessage"] = message;
            }

            return this.View();
        }


        /// <summary>
        /// This action result will reset the authorized user 
        /// and show sign in view
        /// </summary>
        /// <returns> sign up view </returns>
        public ActionResult SignOut()
        {
            //// reset global autorized user 
            Global.AuthorizedUser = null;

            //// remove user cookie
            var cookie = new HttpCookie("user") {Expires = DateTime.Now.AddDays(-1)};
            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

            return this.RedirectToAction("SignIn");
        }

        /// <summary>
        /// This method will check if an user has a timeout
        /// and if so, it will set a tempdate message
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool TimeoutCheck(User user)
        {
            if (user.HasTimeout)
            {
                //// return view with time out message
                var timeOutFor = user.TimeoutTill.Value.Subtract(DateTime.Now);
                var message = string.Format(
                    "<br>U hebt een <b>time-out</b> en u kunt uw account tijdelijk niet gebruiken! <br> De <i>time-out</i> duurt: {0} {1} {2}",
                               timeOutFor.Days < 1 ? string.Empty : timeOutFor.Days.ToString() + (timeOutFor.Days == 1 ? " dag" : " dagen"),
                               timeOutFor.Hours < 1 ? string.Empty : timeOutFor.Hours.ToString() + (timeOutFor.Hours == 1 ? " uur" : " uren"),
                               timeOutFor.Minutes < 1 ? string.Empty : timeOutFor.Minutes.ToString() + (timeOutFor.Minutes == 1 ? " minuut" : " minuten"));

                this.TempData["RemodalMessage"] = message;
                return true;
            }

            return false;
        }
    }
}