﻿//-----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System.Web.Mvc;
using System.Web.Routing;

namespace Asp
{
    /// <summary>
    /// This class contains all elements to create
    /// routes
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Gets or sets the register routes
        /// </summary>
        /// <param name="routes">RouteCollection type routes parameter</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes(); ////Enables Attribute Routing

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = "Start",
                    action = "SignIn",
                    id = UrlParameter.Optional
                });
        }
    }
}
