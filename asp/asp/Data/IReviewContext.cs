﻿//-----------------------------------------------------------------------
// <copyright file="IReviewContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using asp.Models;
using Asp.Models.Users;
using System.Collections.Generic;

namespace asp.Data
{
    /// <summary>
    /// This interface contains methods for the 'ReviewOracleContext.cs'
    /// </summary>
    public interface IReviewContext
    {
        /// <summary>
        /// This method will try and return the next sequence value for a new review
        /// </summary>
        /// <returns>Next review sequence/exception</returns>
        int GetSequenceNextVal();

        /// <summary>
        /// This method will post a review
        /// </summary>
        /// <param name="review"> review type review </param>
        /// <returns> true/ exception </returns>
        bool PostReview(Review review);

        /// <summary>
        /// This method will get a list of reviews
        /// beloning to a single volunteer
        /// </summary>
        /// <param name="volunteerEmail"> string type volunteer's email </param>
        /// <returns> a list of reviews</returns>
        List<Review> GetReviewsFromVolunteer(string volunteerEmail);

        /// <summary>
        /// This method will search for the avg rating 
        /// from a specific volunteer
        /// </summary>
        /// <param name="volunteer"> volunteer type volunteer </param>
        /// <returns> int type avg rating </returns>
        int AvgRatingVolunteer(Volunteer volunteer);
    }
}
