﻿//-----------------------------------------------------------------------
// <copyright file="IUserContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using Asp.Logic;
using Asp.Models.Users;

namespace Asp.Data
{
    /// <summary>
    /// This interface contains methods for the 'UserOracleContext.cs'
    /// </summary>
    public interface IUserContext
    {
        /// <summary>
        /// This method will try and find a user
        /// based on the input (email and password)
        /// if one is found, it will return this user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <returns>A user or an exception</returns>
        User AuthorizedUser(string email, string password);

        /// <summary>
        /// This method will try and get a user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A user or an exception</returns>
        User GetUser(string email);

        /// <summary>
        /// This method will get all helpseekers
        /// </summary>
        /// <returns> list helpseekers </returns>
        List<HelpSeeker> GetAllHelpSeekers();

        /// <summary>
        /// This method will get all volunteers
        /// </summary>
        /// <returns> list volunteers </returns>    
        List<Volunteer> GetAllVolunteers(); 

        /// <summary>
        /// This method will try and register a new user
        /// inside the database (after some checks)
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <returns>True when the user is registered</returns>
        bool Register(User user);

        /// <summary>
        /// This method will try and edit a user's
        /// profile information
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <param name="oldName">string type old name parameter</param>
        /// <returns>True when the user's information is edited</returns>
        bool EditProfileInfo(User user, string oldName);

        /// <summary>
        /// This method will try and edit a help seekers
        /// profile about information        
        /// </summary>
        /// <param name="helpSeeker">HelpSeeker type helpSeeker parameter</param>
        /// <returns>A HelpSeeker or an exception</returns>
        bool EditProfileAboutHelpSeeker(HelpSeeker helpSeeker);

        /// <summary>
        /// This method will try and edit a volunteer
        /// profile about information        
        /// </summary>
        /// <param name="volunteer">Volunteer type volunteer parameter</param>
        /// <returns>A Volunteer or an exception</returns>
        bool EditProfileAboutVolunteer(Volunteer volunteer);

        /// <summary>
        /// This method will edit the planning of a volunteer
        /// </summary>
        /// <param name="availabilities">A string array type 
        /// availabilities parameter containing seven availabilities, 
        /// one for each day of the week, starting at monday</param>
        /// <returns> true/ exception</returns>
        bool EditPlanning(string[] availabilities);

        /// <summary>
        /// This method will set a timeout on a user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="timeout"></param>
        /// <returns> true/ exception </returns>
        bool SetTimeout(string email, DateTime timeout);

        /// <summary>
        /// This method will accept a volunteer in the system
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns> true/ exception </returns>
        bool AcceptVolunteerVog(string email);
    }
}