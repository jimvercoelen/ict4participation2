﻿//-----------------------------------------------------------------------
// <copyright file="QuestionOracleContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using Asp.Models;

namespace Asp.Data
{
    /// <summary>
    /// This class contains all methods from 'IQuestionContext' interface
    /// these methods are all for communicating with the database
    /// </summary>
    public class QuestionOracleContext : IQuestionContext
    {
        /// <summary>
        /// This method will try and return the next sequence value for a new question
        /// </summary>
        /// <returns>Next question sequence/exception</returns>
        public int GetSequenceNextVal()
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "select seq_question.nextval from dual";

                try
                {
                    var seqVal = Convert.ToInt32(command.ExecuteScalar());
                    return seqVal;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method try and insert a new question into the database 
        /// </summary>
        /// <param name="question">Question type question parameters</param>
        /// <returns>Boolean which if true if the inserting succeeded</returns>
        public bool AskQuestion(Question question)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO QUESTION VALUES (:id, :email, NULL, :title, :content, :dateTime, null, " +
                                    "null, null, :city, 'OPEN', :transport)";

                command.Parameters.AddWithValue("id", question.Id);
                command.Parameters.AddWithValue("email", question.HelpSeekerEmail);
                command.Parameters.AddWithValue("title", question.Title);
                command.Parameters.AddWithValue("content", question.Content);
                command.Parameters.AddWithValue("dateTime", question.PostDate);
                command.Parameters.AddWithValue("city", question.Location);
                command.Parameters.AddWithValue("transport", question.Transport);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and render all questions if there a any in the database
        /// </summary>
        /// <returns>A list containing the questions/exception</returns>
        public List<Question> GetQuestions()
        {
            var questions = new List<Question>();
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * " +
                                      "FROM QUESTION " +
                                      "ORDER BY status";

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var question = GlobalStaticOracleContext.GetQuestionFromReader(connection, reader);
                            questions.Add(question);
                        }

                        return questions;
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and render all questions from a specific help seeker
        /// if there a any in the database
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A list containing the questions/exception</returns>
        public List<Question> GetQuestionsFromHelpseeker(string email)
        {
            var questions = new List<Question>();
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * " +
                                      "FROM QUESTION " +
                                      "WHERE helpseeker_email = :email " +
                                      "ORDER BY status";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var question = GlobalStaticOracleContext.GetQuestionFromReader(connection, reader);
                            questions.Add(question);
                        }

                        return questions;
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and render a question depending on question id
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <returns>A single question instance/exception</returns>
        public Question RenderQuestion(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will try and edit a question
        /// </summary>
        /// <param name="question">Question type question parameter</param>
        /// <returns>True when the question is edited</returns>
        public bool EditQuestion(Question question)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE QUESTION " +
                                      "SET title = :title, content = :content, city = :city, transport = :transport " +
                                      "WHERE id = :questionId";

                command.Parameters.AddWithValue("questionId", question.Id);
                command.Parameters.AddWithValue("title", question.Title);
                command.Parameters.AddWithValue("content", question.Content);
                command.Parameters.AddWithValue("city", question.Location);
                command.Parameters.AddWithValue("transport", question.Transport);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and delete a question depending on the question id
        /// </summary>
        /// <param name="id">integer type id parameter</param>
        /// <returns>True when the question is deleted</returns>
        public bool DeleteQuestion(int id)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM QUESTION " +
                                      "WHERE id = :questionId";

                command.Parameters.AddWithValue("questionId", id);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and add an help offer to a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the help is offered</returns>
        public bool OfferHelp(int questionId, string volunteerEmail)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO QUESTION_OFFER VALUES( :questionId, :volunteerEmail )";

                command.Parameters.AddWithValue("questionId", questionId);
                command.Parameters.AddWithValue("volunteerEmail", volunteerEmail);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and remove an help offer from a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the offered help is deleted</returns>
        public bool DeleteOfferedHelp(int questionId, string volunteerEmail)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM QUESTION_OFFER " +
                                      "WHERE question_id = :questionId " +
                                      "AND volunteer_email = :volunteerEmail ";

                command.Parameters.AddWithValue("questionId", questionId);
                command.Parameters.AddWithValue("volunteerEmail", volunteerEmail);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will accept a volunteer to a question
        /// </summary>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <returns>True when the volunteer is accepted</returns>
        public bool AcceptVolunteer(int questionId, string volunteerEmail)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "BEGIN " +
                                        "UPDATE QUESTION " +
                                            "SET volunteer_email = :volunteerEmail, status = 'INPROGRESS' " +
                                            "WHERE id = :questionId; " +

                                        "DELETE FROM QUESTION_OFFER " +
                                            "WHERE question_id = :questionId; " +
                                      "END; ";

                command.Parameters.AddWithValue("questionId", questionId);
                command.Parameters.AddWithValue("volunteerEmail", volunteerEmail);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}