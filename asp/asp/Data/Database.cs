﻿//-----------------------------------------------------------------------
// <copyright file="Database.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System.Data.OracleClient;

namespace Asp.Data
{
    /// <summary>
    /// This class contains database configurations
    /// </summary>
    public class Database
    {
        /// <summary>
        /// This static string represents the database user name
        /// </summary>
        private static string user = "dbi335167";

        /// <summary>
        /// This static string represents the database password
        /// </summary>
        private static string pw = "gRRnXgzAhb";

        /// <summary>
        /// This static string represents the database location
        /// </summary>
        private static string dataSource = " //192.168.15.50:1521/fhictora";

        /// <summary>
        /// This static string represents the connection string
        /// </summary>
        private static string connectionString = "User Id=" + user + ";Password=" + pw + ";Data Source=" + dataSource + ";";

        /// <summary>
        /// Gets connection with the database
        /// </summary>
#pragma warning disable 618
        public static OracleConnection Connection
#pragma warning restore 618
        {
            get
            {
#pragma warning disable 618
                var connection = new OracleConnection(connectionString);
#pragma warning restore 618
                connection.Open();
                return connection;
            }
        }
    }
}