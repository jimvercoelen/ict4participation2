﻿//-----------------------------------------------------------------------
// <copyright file="ReviewOracleContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using asp.Models;
using Asp.Data;
using Asp.Models.Users;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;

namespace asp.Data
{
    /// <summary>
    /// This class contains all methods from 'IReviewContext' interface
    /// these methods are all for communicating with the database
    /// </summary>
    public class ReviewOracleContext : IReviewContext
    {
        /// <summary>
        /// This method will try and return the next sequence value for a new review
        /// </summary>
        /// <returns>Next review sequence/exception</returns>
        public int GetSequenceNextVal()
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "select seq_review.nextval from dual";

                try
                {
                    var seqVal = Convert.ToInt32(command.ExecuteScalar());
                    return seqVal;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will post a review
        /// </summary>
        /// <param name="review"> review type review </param>
        /// <returns> true/ exception </returns>
        public bool PostReview(Review review)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO REVIEW " +
                                      "VALUES( :id, :helpseekerName, :volunteerEmail, :content, :rating, :dateTime )";

                command.Parameters.AddWithValue("id", review.Id);
                command.Parameters.AddWithValue("helpseekerName", review.HelpSeekerName);
                command.Parameters.AddWithValue("volunteerEmail", review.VolunteerEmail);
                command.Parameters.AddWithValue("content", review.Content);
                command.Parameters.AddWithValue("rating", review.Rating);
                command.Parameters.AddWithValue("dateTime", review.PostDate);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will get a list of reviews
        /// beloning to a single volunteer
        /// </summary>
        /// <param name="volunteerEmail"> string type volunteer's email </param>
        /// <returns> a list of reviews</returns>
        public List<Review> GetReviewsFromVolunteer(string volunteerEmail)
        {
            var reviews = new List<Review>();

            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM REVIEW " +
                                      "WHERE volunteer_email = :volunteerEmail";

                command.Parameters.AddWithValue("volunteerEmail", volunteerEmail);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var review = new Review(
                                Convert.ToString(reader["helpseeker_name"]),
                                Convert.ToString(reader["content"]),
                                Convert.ToInt32(reader["rating"]));

                            reviews.Add(review);
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return reviews;
        }

        /// <summary>
        /// This method will search for the avg rating 
        /// from a specific volunteer
        /// </summary>
        /// <param name="volunteer"> volunteer type volunteer </param>
        /// <returns> int type avg rating </returns>
        public int AvgRatingVolunteer(Volunteer volunteer)
        {
            throw new NotImplementedException();
        }
    }
}
