﻿using System.Collections.Generic;
using Asp.Models;

namespace Asp.Data
{
    public interface IChatContext
    {
        Chat GetChatFromPerson(string email, string name);
        List<Chat> GetAllChatsFromPerson(string email);
        List<Message> GetMessagesFromChat(int chatId);
        bool OpenNewChat(string volunteerEmail, string volunteerName, string helpSeekerEmail, string helpSeekerName);
        bool PostMessage(Message message, int chatId);
    }
}