﻿//-----------------------------------------------------------------------
// <copyright file="ImportExportFileOracleContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------

using System;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Web.Mvc;
using Asp.Data;
using asp.Models;
using Asp.Models;

namespace asp.Data
{
    public class ImportExportFileOracleContext
    {
        /// <summary>
        /// imports any file to the Oracle 'Files' table
        /// </summary>
        /// <param name="uploadDate">fate of upload</param>
        /// <param name="fileName">file name</param>
        /// <param name="filePath">file path on the server</param>
        /// <param name="fileType">file type</param>
        /// <param name="fileSize">file size in bytes</param>
        /// <param name="email">users email</param>
        /// <param name="defaultImage">default image bytes array (only when user is registered)</param>
        /// <param name="type">string which defines what stored procedure needs to be used</param>
        /// <returns>if from inserted row</returns>
        public static int ImportFile(
            DateTime uploadDate,
            string fileName,
            string filePath,
            string fileType,
            int fileSize,
            string email,
            byte[] defaultImage,
            string type)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                var transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                //// creates a temporary blob object on the database. This object will store the file content
                command.CommandText = "declare xx blob; begin dbms_lob.createtemporary(xx, false, 0); :tempblob := xx; end;";
                command.Parameters.Add(new OracleParameter("tempblob", OracleType.Blob)).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();

                //// getting the content of the file
                var buffer = defaultImage ?? Global.GetFileContent(filePath);

                //// oracle object responsible for storing the File content
                var tempLob = (OracleLob)command.Parameters[0].Value;
                tempLob.BeginBatch(OracleLobOpenMode.ReadWrite);

                //// write the file content to tempLop
                tempLob.Write(buffer, 0, buffer.Length);
                tempLob.EndBatch();

                command.Parameters.Clear();

                //// the name of the Procedure responsible for inserting the date in the table
                if (type == "IMPORT_FILE")
                {
                    command.CommandText = "IMPORT_FILE";
                }
                else
                {
                    command.CommandText = "IMPORT_VOG";
                }
                
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new OracleParameter("v_upload_date", OracleType.DateTime)).Value = uploadDate;
                command.Parameters.Add(new OracleParameter("v_content", OracleType.Blob)).Value = tempLob;
                command.Parameters.Add(new OracleParameter("v_type", OracleType.VarChar)).Value = fileType;
                command.Parameters.Add(new OracleParameter("v_name", OracleType.VarChar)).Value = fileName;
                command.Parameters.Add(new OracleParameter("v_length", OracleType.Number)).Value = fileSize;
                command.Parameters.Add(new OracleParameter("v_email", OracleType.VarChar)).Value = email;
                command.Parameters.Add(new OracleParameter("v_id", OracleType.Number)).Direction = ParameterDirection.Output;

                try
                {
                    command.ExecuteScalar();
                }
                catch (OracleException e)
                {
                    transaction.Rollback();
                    throw new Exception(e.Message);
                }

                transaction.Commit();

                //// return the id of the inserted row
                return int.Parse(command.Parameters[4].Value.ToString());
            }
        }

        /// <summary>
        /// Gets the correct file 
        /// </summary>
        /// <param name="email"> email to get user image from</param>
        /// <returns> blob file</returns>
        public static FileModel ExportFile(string email, string type)
        {
            var file = new FileModel();

            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                if (type == "IMPORT_FILE")
                {
                    command.CommandText = "SELECT content, f.name, type " +
                                      "FROM FILES f, PERSON p " +
                                      "WHERE p.FILE_ID = f.ID " +
                                      "AND p.EMAIL = :email " +
                                      "AND ROWNUM = 1 " +
                                      "ORDER BY UPLOAD_DATE ";
                }
                else
                {
                    command.CommandText = "SELECT content, f.name, type " +
                                      "FROM FILES f, PERSON_VOLUNTEER pv " +
                                      "WHERE pv.VOG_ID = f.ID " +
                                      "AND pv.PERSON_EMAIL = :email " +
                                      "AND ROWNUM = 1 " +
                                      "ORDER BY UPLOAD_DATE ";
                }

#pragma warning disable 618
                command.Parameters.Add("email", email);
#pragma warning restore 618

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var blob = reader.GetOracleLob(0);
                            var fileName = reader.GetOracleString(1);
                            var fileType = reader.GetOracleString(2);

                            var fileContent = new byte[blob.Length];
                            blob.Read(fileContent, 0, (int)blob.Length);

                            file.FileContent = fileContent;
                            file.FileName = fileName.Value;
                            file.FileType = fileType.Value;
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return file;
        }

        public static bool PDFDisplay(byte[] fileContent, string email)
        {
            try
            {
                if (!Directory.Exists(@"C:\ICT4Participation\VOGs"))
                {
                    Directory.CreateDirectory(@"C:\ICT4Participation\VOGs");
                }

                if (File.Exists(@"C:\ICT4Participation\VOGs\VOG " + email + ".pdf"))
                {
                    File.Delete(@"C:\ICT4Participation\VOGs\VOG " + email + ".pdf");
                }

                File.WriteAllBytes(@"C:\ICT4Participation\VOGs\VOG " + email + ".pdf", fileContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}