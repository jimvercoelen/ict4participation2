﻿//-----------------------------------------------------------------------
// <copyright file="IQuestionContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using Asp.Logic;
using Asp.Models;

namespace Asp.Data
{
    /// <summary>
    /// This interface contains methods for the 'QuestionOracleContext.cs'
    /// </summary>
    public interface IQuestionContext
    {
        /// <summary>
        /// This method will try and return the next sequence value for a new question
        /// </summary>
        /// <returns>Next question sequence/exception</returns>
        int GetSequenceNextVal();

        /// <summary>
        /// This method try and insert a new question into the database 
        /// </summary>
        /// <param name="question">Question type question parameters</param>
        /// <returns>Boolean which if true if the inserting succeeded</returns>
        bool AskQuestion(Question question);

        /// <summary>
        /// This method will try and render all questions if there a any in the database
        /// </summary>
        /// <returns>A list containing the questions/exception</returns>
        List<Question> GetQuestions();

        /// <summary>
        /// This method will try and render all questions from a specific help seeker
        /// if there a any in the database
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A list containing the questions/exception</returns>
        List<Question> GetQuestionsFromHelpseeker(string email);

        /// <summary>
        /// This method will try and render a question depending on question id
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <returns>A single question instance/exception</returns>
        Question RenderQuestion(int questionId);

        /// <summary>
        /// This method will try and edit a question
        /// </summary>
        /// <param name="question">Question type question parameter</param>
        /// <returns>True when the question is edited</returns>
        bool EditQuestion(Question question);

        /// <summary>
        /// This method will try and delete a question depending on the question id
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <returns>True when the question is deleted</returns>
        bool DeleteQuestion(int questionId);

        /// <summary>
        /// This method will try and add an help offer to a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the help is offered</returns>
        bool OfferHelp(int questionId, string volunteerEmail);

        /// <summary>
        /// This method will try and remove an help offer from a question
        /// </summary>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <returns>True when the offered help is deleted</returns>
        bool DeleteOfferedHelp(int questionId, string volunteerEmail);

        /// <summary>
        /// This method will accept a volunteer to a question
        /// </summary>
        /// <param name="volunteerEmail">string type volunteerEmail parameter</param>
        /// <param name="questionId">integer type questionId parameter</param>
        /// <returns>True when the volunteer is accepted</returns>
        bool AcceptVolunteer(int questionId, string volunteerEmail);
    }
}