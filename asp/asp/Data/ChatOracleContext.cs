﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using Asp.Models;

namespace Asp.Data
{
    public class ChatOracleContext : IChatContext
    {
        /// <summary>
        /// This method will try and return the next sequence value for a new chat
        /// </summary>
        /// <returns>Next chat sequence or exception</returns>
        public int GetSequenceNextValChat()
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "select seq_chat.nextval from dual";

                try
                {
                    var seqVal = Convert.ToInt32(command.ExecuteScalar());
                    return seqVal;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public Chat GetChatFromPerson(string email, string name)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT pc.* " +
                                        "FROM CHAT c, PERSON_CHAT pc, PERSON p " +
                                        "WHERE c.CHAT_ID = pc.CHAT_ID " +
                                        "AND pc.PERSON_EMAIL = p.EMAIL " +
                                        "AND pc.PERSON_EMAIL=:email " +
                                        "AND pc.TITLE=:name";

                command.Parameters.AddWithValue("email", email);
                command.Parameters.AddWithValue("name", name);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                return new Chat(Convert.ToInt32(reader["CHAT_ID"]),
                                                Convert.ToString(reader["TITLE"]));
                            }
                        }
                    }
                }

                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        public List<Chat> GetAllChatsFromPerson(string email)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                var chats = new List<Chat>();

                command.CommandText = "SELECT pc.* " +
                                       "FROM CHAT c, PERSON_CHAT pc " +
                                       "WHERE c.chat_id = pc.chat_id " +
                                       "AND pc.person_email =: email";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                chats.Add(new Chat(
                                    Convert.ToInt32(reader["chat_id"]), 
                                    Convert.ToString(reader["title"])));
                            }
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }

                return chats;
            }
        }

        /// <summary>
        /// This method will try and return the next sequence value for a new message
        /// </summary>
        /// <returns>Next message sequence or exception</returns>
        public int GetSequenceNextValMessage()
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "select seq_message.nextval from dual";

                try
                {
                    var seqVal = Convert.ToInt32(command.ExecuteScalar());
                    return seqVal;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public bool OpenNewChat(
            string volunteerEmail, 
            string volunteerName, 
            string helpSeekerEmail, 
            string helpSeekerName)
        {
            var sql1 = "";
            var sql2 = "";
            var sql3 = "";

            // create new chat
            sql1 = "BEGIN " +
                        "INSERT INTO CHAT (CHAT_ID) " +
                        "VALUES (:chatId); ";

            // add volunteer to PERSON_CHAT
            sql2 = "INSERT INTO PERSON_CHAT (PERSON_EMAIL, CHAT_ID, TITLE) " +
                        "VALUES (:volunteerEmail, :chatId, :helpName); ";

            // add helpseeker to PERSON_CHAT
            sql3 = "INSERT INTO PERSON_CHAT (PERSON_EMAIL, CHAT_ID, TITLE) " +
                    "VALUES (:helpseekerEmail, :chatId, :volName); " +
                    "END;";

            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {

                command.CommandText = string.Format("{0}{1}{2}", sql1, sql2, sql3);

                command.Parameters.AddWithValue("chatId", GetSequenceNextValChat());
                command.Parameters.AddWithValue("volName", volunteerName);
                command.Parameters.AddWithValue("helpName", helpSeekerName);
                command.Parameters.AddWithValue("volunteerEmail", volunteerEmail);
                command.Parameters.AddWithValue("helpseekerEmail", helpSeekerEmail);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public List<Message> GetMessagesFromChat(int chatId)
        {
            var messages = new List<Message>();
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {

                command.CommandText = "SELECT m.*, p.NAME " +
                                       "FROM MESSAGE m, CHAT c, PERSON p " +
                                       "WHERE m.CHAT_ID = c.CHAT_ID " +
                                       "AND c.CHAT_ID = :chatID " +
                                       "AND p.NAME = m.PERSON_NAME " +
                                       "ORDER BY date_time ASC";

                command.Parameters.AddWithValue("chatID", chatId);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            messages.Add(new Message(
                                Convert.ToInt32(reader["message_id"]),
                                Convert.ToString(reader["name"]),
                                Convert.ToString(reader["content"]),
                                Convert.ToDateTime(reader["date_time"])));
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }

                return messages;
            }
        }

        public bool PostMessage(Message message, int chatId)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {

                command.CommandText = "INSERT INTO MESSAGE " +
                                      "VALUES(:messageId, :personName, :chatId, :content, :dateTime)";

                command.Parameters.AddWithValue("messageId", GetSequenceNextValMessage());
                command.Parameters.AddWithValue("personName", message.Name);
                command.Parameters.AddWithValue("chatId", chatId);
                command.Parameters.AddWithValue("content", message.Content);
                command.Parameters.AddWithValue("dateTime", message.DateTime);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}