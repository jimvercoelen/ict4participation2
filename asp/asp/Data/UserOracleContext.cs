﻿//-----------------------------------------------------------------------
// <copyright file="UserOracleContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using Asp.Models;
using Asp.Models.Users;

namespace Asp.Data
{
    /// <summary>
    /// This class contains all methods from 'IUserContext' interface
    /// these methods are all for communicating with the database
    /// </summary>
    public class UserOracleContext : IUserContext
    {
        /// <summary>
        /// This method will try and find a user
        /// based on the input (email and password)
        /// if one is found, it will return this user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <param name="password">string type password parameter</param>
        /// <returns>A user or an exception</returns>
        public User AuthorizedUser(string email, string password)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM PERSON " +
                                      "WHERE email = :email " +
                                      "AND password = :password";

                command.Parameters.AddWithValue("email", email);
                command.Parameters.AddWithValue("password", password);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            switch (Convert.ToString(reader["user_kind"]).ToUpper())
                            {
                                case "ADMIN":
                                    return new Admin(UserType.Admin, email, password);

                                case "HELPSEEKER":
                                    return GlobalStaticOracleContext.GetHelpseeker(connection, email);
                                    
                                case "VOLUNTEER":
                                    return GlobalStaticOracleContext.GetVolunteer(connection, email);
                            }
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return null;
        }

        /// <summary>
        /// This method will try and get a user
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns>A user or an exception</returns>
        public User GetUser(string email)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM PERSON " +
                                      "WHERE email = :email ";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            switch (Convert.ToString(reader["user_kind"]).ToUpper())
                            {
                                case "ADMIN":
                                    return new Admin(
                                        UserType.Admin, 
                                        Convert.ToString(reader["email"]),
                                        Convert.ToString(reader["password"]));
                                case "HELPSEEKER":
                                    return GlobalStaticOracleContext.GetHelpseeker(connection, email);

                                case "VOLUNTEER":
                                    return GlobalStaticOracleContext.GetVolunteer(connection, email);
                            }
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return null;
        }

        /// <summary>
        /// This method will get all helpseekers
        /// </summary>
        /// <returns> list helpseekers </returns>
        public List<HelpSeeker> GetAllHelpSeekers()
        {
            var helpSeekers = new List<HelpSeeker>();
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM PERSON " +
                                      "WHERE UPPER(user_kind) = 'HELPSEEKER' ";
                
                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var email = Convert.ToString(reader["email"]);
                            helpSeekers.Add(GlobalStaticOracleContext.GetHelpseeker(connection, email));
                        }
                    }

                    return helpSeekers;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will get all volunteers
        /// </summary>
        /// <returns> list volunteers </returns>  
        public List<Volunteer> GetAllVolunteers()
        {
            var volunteers = new List<Volunteer>();
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM PERSON " +
                                      "WHERE UPPER(user_kind) = 'VOLUNTEER' ";
                
                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var email = Convert.ToString(reader["email"]);
                            volunteers.Add(GlobalStaticOracleContext.GetVolunteer(connection, email));
                        }
                    }

                    return volunteers;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and register a new user
        /// inside the database (after some checks)
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <returns>True when the user is registered</returns>
        public bool Register(User user)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                var query = "BEGIN " +
                            "INSERT INTO PERSON VALUES ( :email, :password, :usertype, " +
                            "null, null, null, null, null, null, null, null, default, null, 61 ); ";

                switch (user.UserType)
                {
                    case UserType.Helpseeker:
                        query += "INSERT INTO PERSON_HELP_SEEKER VALUES ( :email, default ); " +
                                 "END; ";
                        break;
                    case UserType.Volunteer:
                        query += "INSERT INTO PERSON_VOLUNTEER VALUES ( :email, default, default, default, null ); " +
                                 "END;";
                        break;
                }

                command.CommandText = query;

                command.Parameters.AddWithValue("email", user.Email);
                command.Parameters.AddWithValue("password", user.Password);
                command.Parameters.AddWithValue("usertype", user.UserType.ToString());

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and edit a user's
        /// profile information
        /// </summary>
        /// <param name="user">User type user parameter</param>
        /// <param name="oldName">string type old name</param>
        /// <returns>True when the user's information is edited</returns>
        public bool EditProfileInfo(User user, string oldName)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "BEGIN " +
                                        "UPDATE PERSON " +
                                        "SET name = :name, bio = :bio " +
                                        "WHERE email = :email; " +
                                      
                                        "UPDATE MESSAGE " +
                                        "SET person_name = :name " +
                                        "WHERE person_name = :oldName; " +
                                      "END;";

                command.Parameters.AddWithValue("email", Global.AuthorizedUser.Email);
                command.Parameters.AddWithValue("name", user.Name);
                command.Parameters.AddWithValue("bio", user.Bio);
                command.Parameters.AddWithValue("oldName", oldName);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and edit a help seekers
        /// profile about information        
        /// </summary>
        /// <param name="helpSeeker">HelpSeeker type helpSeeker parameter</param>
        /// <returns>A HelpSeeker or an exception</returns>
        public bool EditProfileAboutHelpSeeker(HelpSeeker helpSeeker)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {           
                command.CommandText = 
                    "BEGIN " +
                    "UPDATE PERSON " +
                    "SET date_of_birth = :dateOfBirth, city = :city " +
                    "WHERE email = :email; " +
                    
                    "UPDATE PERSON_HELP_SEEKER " +
                    "SET can_use_public_transport = :publicTransport " +
                    "WHERE person_email = :email; " +
                    "END; "; 

                command.Parameters.AddWithValue("dateOfBirth", helpSeeker.DateOfBirth);
                command.Parameters.AddWithValue("city", helpSeeker.Location);
                command.Parameters.AddWithValue("email", helpSeeker.Email);
                command.Parameters.AddWithValue("publicTransport", Convert.ToInt32(helpSeeker.CanUsePublicTransport));

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and edit a volunteer
        /// profile about information        
        /// </summary>
        /// <param name="volunteer">Volunteer type volunteer parameter</param>
        /// <returns>A Volunteer or an exception</returns>
        public bool EditProfileAboutVolunteer(Volunteer volunteer)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText =
                    "BEGIN " +
                    "UPDATE PERSON " +
                    "SET date_of_birth = :dateOfBirth, city = :city " +
                    "WHERE email = :email; " +

                    "UPDATE PERSON_VOLUNTEER " +
                    "SET has_driving_license = :hasDrivingLicense, has_car = :hasCar " +
                    "WHERE person_email = :email; " +
                    "END; ";

                command.Parameters.AddWithValue("email", volunteer.Email);
                command.Parameters.AddWithValue("dateOfBirth", volunteer.DateOfBirth);
                command.Parameters.AddWithValue("city", volunteer.Location);
                command.Parameters.AddWithValue("hasDrivingLicense", Convert.ToInt32(volunteer.HasDrivingLicense));
                command.Parameters.AddWithValue("hasCar", Convert.ToInt32(volunteer.HasCar));

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will try and edit the planning of a volunteer
        /// </summary>
        /// <param name="availabilities">A string array type 
        /// availabilities parameter containing seven availabilities, 
        /// one for each day of the week, starting at monday</param>
        /// <returns> true/ exception</returns>
        public bool EditPlanning(string[] availabilities)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                var transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                command.CommandText = "EDIT_PLANNING";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new OracleParameter("v_email", OracleType.VarChar)).Value =
                    Global.AuthorizedUser.Email;
                command.Parameters.Add(new OracleParameter("v_mo", OracleType.VarChar)).Value = availabilities[0];
                command.Parameters.Add(new OracleParameter("v_tu", OracleType.VarChar)).Value = availabilities[1];
                command.Parameters.Add(new OracleParameter("v_we", OracleType.VarChar)).Value = availabilities[2];
                command.Parameters.Add(new OracleParameter("v_th", OracleType.VarChar)).Value = availabilities[3];
                command.Parameters.Add(new OracleParameter("v_fr", OracleType.VarChar)).Value = availabilities[4];
                command.Parameters.Add(new OracleParameter("v_sa", OracleType.VarChar)).Value = availabilities[5];
                command.Parameters.Add(new OracleParameter("v_su", OracleType.VarChar)).Value = availabilities[6];

                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method wiil set a timeout on a user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="timeout"></param>
        /// <returns> true/ exception </returns>
        public bool SetTimeout(string email, DateTime timeout)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE PERSON " +
                                      "SET time_out_duration = :timeOutDate " +
                                      "WHERE email = :email";

                command.Parameters.AddWithValue("timeOutDate", timeout);
                command.Parameters.AddWithValue("email", email);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// This method will accept a volunteer in the system
        /// </summary>
        /// <param name="email">string type email parameter</param>
        /// <returns> true/ exception </returns>
        public bool AcceptVolunteerVog(string email)
        {
            using (var connection = Database.Connection)
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE PERSON_VOLUNTEER " +
                                      "SET VOG_ACCEPTED = 1 " +
                                      "WHERE PERSON_EMAIL=:email";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (OracleException e)
                {
                    return false;
                }
            }
        }
    }
}