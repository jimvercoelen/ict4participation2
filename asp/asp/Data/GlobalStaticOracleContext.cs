﻿//-----------------------------------------------------------------------
// <copyright file="GlobalStaticOracleContext.cs" company="ICT4Participation">
//     Copyright (c) ICT4Participation. All rights reserved.
// </copyright>
// <author>ICT4Participation</author>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using asp.Data;
using Asp.Models;
using Asp.Models.Users;

namespace Asp.Data
{
    /// <summary>
    /// This class contains static oracle communication methods.
    /// This way, these methods can be used in multiple 'OracleContext' classes
    /// </summary>
    public static class GlobalStaticOracleContext
    {
        /// <summary>
        /// This method will read a help seeker from the database
        /// </summary>
        /// <param name="connection"> oracle connection</param>
        /// <param name="email"> the help seekers email</param>
        /// <returns> a help seeker instance</returns>
#pragma warning disable 618
        public static HelpSeeker GetHelpseeker(OracleConnection connection, string email)
#pragma warning restore 618
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT p.*, ph.* " +
                                      "FROM PERSON p, PERSON_HELP_SEEKER ph " +
                                      "WHERE p.email = ph.person_email " +
                                      "AND p.email = :email ";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var password = Convert.ToString(reader["password"]);
                            var name = Convert.ToString(reader["name"]);
                            var bio = Convert.ToString(reader["bio"]);
                            var location = Convert.ToString(reader["city"]);
                            var dateOfBirth = reader["date_of_birth"] == DBNull.Value ? null : (DateTime?)reader["date_of_birth"];
                            var timeoutTill = reader["time_out_duration"] == DBNull.Value ? null : (DateTime?)reader["time_out_duration"];
                            var canUsePublicTransport = Convert.ToBoolean(reader["can_use_public_transport"]);
                            var hasTimeout = timeoutTill > DateTime.Now ? true : false;

                            var helpseeker = new HelpSeeker(
                                UserType.Helpseeker, 
                                email,
                                password,
                                name,
                                bio,
                                location,
                                dateOfBirth,
                                timeoutTill,
                                canUsePublicTransport);

                            helpseeker.ProfileImage = ImportExportFileOracleContext.ExportFile(email, "IMPORT_FILE");
                            helpseeker.HasTimeout = hasTimeout;

                            return helpseeker;
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return null;
        }


        /// <summary>
        /// This method will read a volunteer from the database
        /// </summary>
        /// <param name="connection"> oracle connection</param>
        /// <param name="email"> the volunteer email</param>
        /// <returns> a volunteer instance</returns>
#pragma warning disable 618
        public static Volunteer GetVolunteer(OracleConnection connection, string email)
#pragma warning restore 618
        {
            Volunteer volunteer = null;
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT p.*, pv.* " +
                                      "FROM PERSON p, PERSON_VOLUNTEER pv " +
                                      "WHERE p.email = pv.person_email " +
                                      "AND p.email = :email ";

                command.Parameters.AddWithValue("email", email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var password = Convert.ToString(reader["password"]);
                            var name = Convert.ToString(reader["name"]);
                            var bio = Convert.ToString(reader["bio"]);
                            var location = Convert.ToString(reader["city"]);
                            var dateOfBirth = reader["date_of_birth"] == DBNull.Value ? null : (DateTime?)reader["date_of_birth"];
                            var timeoutTill = reader["time_out_duration"] == DBNull.Value ? null : (DateTime?)reader["time_out_duration"];
                            var vogAccepted = Convert.ToInt32(reader["VOG_ACCEPTED"]) == 1;
                            var hasDrivingLicense = Convert.ToBoolean(reader["has_driving_license"]);
                            var hasCar = Convert.ToBoolean(reader["has_car"]);
                            var hasTimeout = timeoutTill > DateTime.Now ? true : false;
                            
                            volunteer = new Volunteer(
                                UserType.Volunteer,
                                email,
                                password,
                                name,
                                bio,
                                location, 
                                dateOfBirth,
                                timeoutTill,
                                hasDrivingLicense,
                                hasCar,
                                vogAccepted);

                            volunteer.ProfileImage = ImportExportFileOracleContext.ExportFile(email, "IMPORT_FILE");
                            volunteer.Vog = ImportExportFileOracleContext.ExportFile(email, "IMPORT_VOG");
                            volunteer.HasTimeout = hasTimeout;
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT daypart " +
                                      "FROM AVAILABILITY " +
                                      "WHERE person_email = :email " +
                                      "ORDER BY DECODE( DAY, 'mo', 1, 'tu', 2, 'we', 3, 'th', 4, 'fr', 5, 'sa', 6, 'su', 7 )";

                command.Parameters.AddWithValue("email", volunteer.Email);

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var i = 0;
                        while (reader.Read())
                        {
                            volunteer.Availabilities[i] = Convert.ToString(reader[0]);
                            i++;
                        }
                    }
                }
                catch (OracleException e)
                {
                    throw new Exception(e.Message);
                }
            }

            return volunteer;
        }

        /// <summary>
        /// This method will create a question from a oracle data reader
        /// </summary>
        /// <param name="connection"> Oracle connection </param>
        /// <param name="reader"> oracle data reader </param>
        /// <returns> a question </returns>
#pragma warning disable 618
        public static Question GetQuestionFromReader(OracleConnection connection, OracleDataReader reader)
#pragma warning restore 618
        {
            var id = Convert.ToInt32(reader["id"]);
            var volunteerEmail = Convert.ToString(reader["volunteer_email"]);

            var question = new Question(
                id,
                Convert.ToString(reader["helpseeker_email"]),
                Convert.ToString(reader["title"]),
                Convert.ToString(reader["content"]),
                Convert.ToDateTime(reader["date_time"]),
                Convert.ToString(reader["city"]),
                Convert.ToString(reader["status"]),
                Convert.ToString(reader["transport"]));

            var status = Convert.ToString(reader["status"]);

            switch (status)
            {
                case "OPEN":
                    question.HelpOfferedVolunteers = GetHelpOfferedVolunteers(connection, id);
                    break;
                case "INPROGRESS":
                    question.AcceptedVolunteer = GetVolunteer(connection, volunteerEmail);
                    break;
            }

            return question;
        }

        /// <summary>
        /// This method will get all volunteers who had offered help on a specific question
        /// </summary>
        /// <param name="connection"> Oracle connection </param>
        /// <param name="questionId"> question id </param>
        /// <returns> list of volunteers </returns>
#pragma warning disable 618
        public static List<Volunteer> GetHelpOfferedVolunteers(OracleConnection connection, int questionId)
#pragma warning restore 618
        {
            var volunteers = new List<Volunteer>();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT volunteer_email " +
                                      "FROM QUESTION_OFFER " +
                                      "WHERE question_id = :questionId ";

                command.Parameters.AddWithValue("questionId", questionId);

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        volunteers.Add(GetVolunteer(connection, Convert.ToString(reader["volunteer_email"])));
                    }

                    return volunteers;
                }
            }
        }
    }
}